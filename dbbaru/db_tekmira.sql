-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2019 at 12:13 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tekmira`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_booking_konfirmasi`
--

CREATE TABLE `tabel_booking_konfirmasi` (
  `id_booking` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telphone` varchar(12) NOT NULL,
  `email` varchar(20) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `tgl_keluar` date NOT NULL,
  `jumlah_kamar` int(3) NOT NULL,
  `id_type` int(10) NOT NULL,
  `id_wisma` int(10) NOT NULL,
  `total` int(11) NOT NULL,
  `kode_invoice` varchar(30) NOT NULL,
  `durasi` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_booking_konfirmasi`
--

INSERT INTO `tabel_booking_konfirmasi` (`id_booking`, `nama`, `telphone`, `email`, `tgl_masuk`, `tgl_keluar`, `jumlah_kamar`, `id_type`, `id_wisma`, `total`, `kode_invoice`, `durasi`, `status`) VALUES
(31, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-02', '2019-08-03', 1, 1, 1, 175000, 'TM304082019001', 1, 1),
(32, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-05', '2019-08-07', 1, 2, 3, 450000, 'TM304082019002', 2, 1),
(33, 'fuad', '0535005963', 'fuad@gmail.com', '2019-08-02', '2019-08-03', 1, 1, 3, 150000, 'TM305082019003', 1, 1),
(34, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-05', '2019-08-08', 1, 1, 1, 450000, 'TM305082019004', 3, 1),
(35, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-09', '2019-08-10', 1, 1, 3, 150000, 'TM305082019005', 1, 1),
(36, 'David Priyadi', '0864446466', 'admin@gmail.com', '2019-08-06', '2019-08-07', 1, 1, 1, 150000, 'TM305082019006', 1, 1),
(37, 'David', '0535005963', 'admin@gmail.com', '2019-08-07', '2019-08-08', 3, 1, 1, 450000, 'TM305082019007', 1, 1),
(38, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-06', '2019-08-08', 0, 3, 1, 0, 'TM305082019008', 2, 2),
(39, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-06', '2019-08-07', 2, 2, 3, 400000, 'TM305082019009', 1, 1),
(40, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-02', '2019-08-03', 2, 1, 1, 300000, 'TM305082019010', 1, 2),
(41, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-07', '2019-08-09', 2, 1, 1, 600000, 'TM305082019010', 2, 2),
(42, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-06', '2019-08-07', 2, 1, 1, 300000, 'TM305082019010', 1, 2),
(43, 'David Priyadi', '8662252662', 'admin@gmail.com', '2019-08-06', '2019-08-09', 3, 1, 1, 1350000, 'TM305082019010', 3, 2),
(44, 'David Priyadi', '087777777', 'admin@gmail.com', '2019-08-02', '2019-08-03', 2, 1, 1, 300000, 'TM306082019010', 1, 1),
(45, 'David Priyadi', '098', 'admin@gmail.com', '2019-08-07', '2019-08-09', 2, 1, 1, 600000, 'TM306082019010', 2, 1),
(46, 'Daffa', '083824106316', 'daffa@gmail.com', '2019-08-07', '2019-08-08', 2, 2, 1, 400000, 'TM306082019010', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wisma`
--

CREATE TABLE `tbl_wisma` (
  `id_wisma` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wisma`
--

INSERT INTO `tbl_wisma` (`id_wisma`, `name`, `alamat`, `no_telp`, `image`) VALUES
(1, 'Wisma Tekmira 1', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'hotel.jpg'),
(2, 'Wisma Tekmira 2', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'hotel2.jpg'),
(3, 'Wisma Tekmira 3', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'hotel3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_book`
--

CREATE TABLE `tb_book` (
  `id_book` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `tgl_keluar` date NOT NULL,
  `jml_kamar` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `kode_invoice` varchar(20) NOT NULL,
  `id_wisma` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `status` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_book`
--

INSERT INTO `tb_book` (`id_book`, `nama`, `email`, `no_hp`, `tgl_masuk`, `tgl_keluar`, `jml_kamar`, `total`, `kode_invoice`, `id_wisma`, `id_type`, `status`) VALUES
(26, 'David Priyadi', 'admin@gmail.com', '0864446466', '2019-08-06', '2019-08-07', 1, 150000, 'TM305082019006', 1, 1, 1),
(27, 'David', 'admin@gmail.com', '0535005963', '2019-08-07', '2019-08-08', 3, 450000, 'TM305082019007', 1, 1, 1),
(28, 'David Priyadi', 'admin@gmail.com', '8662252662', '2019-08-06', '2019-08-08', 2, 800000, 'TM305082019009', 3, 2, 1),
(29, 'David Priyadi', 'admin@gmail.com', '098', '2019-08-07', '2019-08-30', 2, 6900000, 'TM306082019010', 1, 1, 0),
(30, 'Daffa', 'daffa@gmail.com', '083824106316', '2019-08-07', '2019-08-12', 2, 2000000, 'TM306082019010', 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_book`
--

CREATE TABLE `tb_detail_book` (
  `id_detail_book` int(11) NOT NULL,
  `id_book` int(11) NOT NULL,
  `id_kamar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_book`
--

INSERT INTO `tb_detail_book` (`id_detail_book`, `id_book`, `id_kamar`) VALUES
(44, 26, 28),
(45, 27, 29),
(46, 27, 30),
(47, 27, 31),
(48, 28, 64),
(49, 28, 65),
(50, 29, 28),
(51, 29, 29),
(52, 30, 33),
(53, 30, 34);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_wisma` int(11) NOT NULL,
  `no_kamar` varchar(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `id_type`, `id_wisma`, `no_kamar`, `status`) VALUES
(28, 1, 1, 'WS001', 1),
(29, 1, 1, 'WS002', 1),
(30, 1, 1, 'WS003', 0),
(31, 1, 1, 'WS004', 0),
(32, 1, 1, 'WS005', 0),
(33, 2, 1, 'WD001', 1),
(34, 2, 1, 'WD002', 1),
(35, 2, 1, 'WD003', 0),
(36, 2, 1, 'WD004', 0),
(37, 2, 1, 'WD005', 0),
(38, 3, 1, 'WSU01', 0),
(39, 3, 1, 'WSU02', 0),
(40, 3, 1, 'WSU03', 0),
(41, 3, 1, 'WSU04', 0),
(42, 3, 1, 'WSU05', 0),
(43, 1, 2, 'WS201', 0),
(44, 1, 2, 'WS202', 0),
(45, 1, 2, 'WS203', 0),
(46, 1, 2, 'WS204', 0),
(47, 1, 2, 'WS205', 0),
(48, 2, 2, 'WSD201', 0),
(49, 2, 2, 'WSD202', 0),
(50, 2, 2, 'WSD203', 0),
(51, 2, 2, 'WSD204', 0),
(52, 2, 2, 'WSD205', 0),
(53, 3, 2, 'WSU201', 0),
(54, 3, 2, 'WSU202', 0),
(55, 3, 2, 'WSU203', 0),
(56, 3, 2, 'WSU204', 0),
(57, 3, 2, 'WSU205', 0),
(58, 1, 3, 'WS301', 0),
(59, 1, 3, 'WS302', 0),
(60, 1, 3, 'WS303', 0),
(61, 1, 3, 'WS304', 0),
(63, 1, 3, 'WS305', 0),
(64, 2, 3, 'WD301', 0),
(65, 2, 3, 'WD302', 0),
(66, 2, 3, 'WD303', 0),
(67, 2, 3, 'WD304', 0),
(69, 2, 3, 'WD305', 0),
(75, 3, 3, 'WSU301', 0),
(84, 3, 3, 'WSU302', 0),
(85, 3, 3, 'WSU303', 0),
(90, 3, 3, 'WSU304', 0),
(91, 3, 3, 'WSU305', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_perpanjang`
--

CREATE TABLE `tb_perpanjang` (
  `id_perpanjang` int(11) NOT NULL,
  `id_book` int(11) DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `tgl_perpanjang` date DEFAULT NULL,
  `harga_kamar` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tgl_proses` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_perpanjang`
--

INSERT INTO `tb_perpanjang` (`id_perpanjang`, `id_book`, `tanggal_keluar`, `tgl_perpanjang`, `harga_kamar`, `total`, `tgl_proses`) VALUES
(20, 28, '2019-08-07', '2019-08-08', 200000, 400000, '2019-08-05 20:20:39'),
(21, 29, '2019-08-09', '2019-08-10', 150000, 300000, '2019-08-06 22:40:59'),
(22, 29, '2019-08-10', '2019-08-11', 150000, 300000, '2019-08-06 22:42:52'),
(23, 29, '2019-08-11', '2019-08-12', 150000, 300000, '2019-08-06 22:55:43'),
(24, 29, '2019-08-12', '2019-08-18', 150000, 1800000, '2019-08-06 22:57:47'),
(25, 29, '2019-08-18', '2019-08-19', 150000, 300000, '2019-08-06 23:04:17'),
(26, 29, '2019-08-18', '2019-08-19', 150000, 300000, '2019-08-06 23:06:18'),
(27, 29, '2019-08-19', '2019-08-29', 150000, 3000000, '2019-08-06 23:07:38'),
(28, 29, '2019-08-29', '2019-08-30', 150000, 300000, '2019-08-06 23:17:47'),
(29, 30, '2019-08-08', '2019-08-09', 200000, 400000, '2019-08-06 23:22:30'),
(30, 30, '2019-08-09', '2019-08-12', 200000, 1200000, '2019-08-07 00:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_type_kamar`
--

CREATE TABLE `tb_type_kamar` (
  `id_type` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `harga_weekend` int(11) NOT NULL,
  `harga_weekday` int(11) NOT NULL,
  `img_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_type_kamar`
--

INSERT INTO `tb_type_kamar` (`id_type`, `type`, `deskripsi`, `harga_weekend`, `harga_weekday`, `img_type`) VALUES
(1, 'Standar', '<div><br></div><span class=\"fas fa-bed\"></span>  Double Bed                          <span class=\"fas fa-users\"></span>  Double Guest<div><span class=\"fas fa-wifi\"></span>  Free Wifi                               <span class=\"fas fa-parking\"></span>   Free Parking</div>', 175000, 150000, 'Standar.jpg'),
(2, 'Deluxe', '<div><br></div><span class=\"fas fa-bed\"></span>  Double Bed                          <span class=\"fas fa-users\"></span>  Double Guest<div><span class=\"fas fa-wifi\"></span>  Free Wifi                               <span class=\"fas fa-parking\"></span>   Free Parking</div>', 225000, 200000, 'Dexluc.jpg'),
(3, 'Superior', '<div><br></div><i class=\"fas fa-bed\"></i>  Double Bed                          <i class=\"fas fa-users\"></i>  Double Guest<div><i class=\"fas fa-wifi\"></i>  Free Wifi                               <i class=\"fas fa-parking\"></i>   Free Parking</div>', 200000, 175000, 'Superior.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(14, 'david', 'davidpriyadi77@gmail.com', 'kamar.jpg', '$2y$10$Y9S9..WEsWudycsWofNeIe.DvyWb0uSHPiPVHXuoEEU5WIqY8YlTK', 1, 1, 1562167212);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(10, 1, 5),
(21, 1, 4),
(27, 1, 7),
(28, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Kamar'),
(5, 'Booking'),
(6, 'Bukti Pembayaran'),
(7, 'Wisma'),
(8, 'Setting');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member'),
(3, 'Wisma'),
(4, 'Gedung'),
(5, 'Ruangan');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'My Profile', 'user', 'fas fa-fw fa-user', 1),
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(7, 1, 'Role', 'admin/role', 'fas fa-fw fa-user-tie', 1),
(8, 2, 'Change Password', 'user/changepassword', 'fas fa-fw fa-key', 1),
(9, 4, 'Kamar', 'kamar/kamar', 'fas fa-fw fa-person-booth', 1),
(11, 4, 'Kelas Kamar', 'kamar/kelaskamar', 'fas fa-fw fa-star', 1),
(12, 5, 'Pemesanan Baru', 'booking/pemesananbaru', 'fas fa-fw fa-calendar-alt', 1),
(13, 5, 'Daftar Booking', 'booking/listbooking', 'fas fa-fw fa-calendar-check', 1),
(14, 6, 'Bukti Transfer', 'buktipembayaran', 'fas fa-fw fa-receipt', 1),
(15, 7, 'Wisma', 'wisma', 'fas fa-fw fa-calendar-alt', 1),
(16, 5, 'Daftar CekOut', 'booking/listchekout', 'fas fa-fw  fa-door-open', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_booking_konfirmasi`
--
ALTER TABLE `tabel_booking_konfirmasi`
  ADD PRIMARY KEY (`id_booking`),
  ADD KEY `id_type` (`id_type`),
  ADD KEY `id_wisma` (`id_wisma`);

--
-- Indexes for table `tbl_wisma`
--
ALTER TABLE `tbl_wisma`
  ADD PRIMARY KEY (`id_wisma`);

--
-- Indexes for table `tb_book`
--
ALTER TABLE `tb_book`
  ADD PRIMARY KEY (`id_book`);

--
-- Indexes for table `tb_detail_book`
--
ALTER TABLE `tb_detail_book`
  ADD PRIMARY KEY (`id_detail_book`);

--
-- Indexes for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`),
  ADD KEY `id_type` (`id_type`),
  ADD KEY `id_wisma` (`id_wisma`);

--
-- Indexes for table `tb_perpanjang`
--
ALTER TABLE `tb_perpanjang`
  ADD PRIMARY KEY (`id_perpanjang`);

--
-- Indexes for table `tb_type_kamar`
--
ALTER TABLE `tb_type_kamar`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_booking_konfirmasi`
--
ALTER TABLE `tabel_booking_konfirmasi`
  MODIFY `id_booking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tbl_wisma`
--
ALTER TABLE `tbl_wisma`
  MODIFY `id_wisma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_book`
--
ALTER TABLE `tb_book`
  MODIFY `id_book` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_detail_book`
--
ALTER TABLE `tb_detail_book`
  MODIFY `id_detail_book` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tb_perpanjang`
--
ALTER TABLE `tb_perpanjang`
  MODIFY `id_perpanjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_type_kamar`
--
ALTER TABLE `tb_type_kamar`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tabel_booking_konfirmasi`
--
ALTER TABLE `tabel_booking_konfirmasi`
  ADD CONSTRAINT `tabel_booking_konfirmasi_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `tb_type_kamar` (`id_type`),
  ADD CONSTRAINT `tabel_booking_konfirmasi_ibfk_2` FOREIGN KEY (`id_wisma`) REFERENCES `tbl_wisma` (`id_wisma`);

--
-- Constraints for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD CONSTRAINT `tb_kamar_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `tb_type_kamar` (`id_type`),
  ADD CONSTRAINT `tb_kamar_ibfk_2` FOREIGN KEY (`id_wisma`) REFERENCES `tbl_wisma` (`id_wisma`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
