<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{

        $this->load->view('templates/home_header');
        $this->load->view('home/index');
        $this->load->view('templates/home_footer');
    } 
    
    public function galleryPhoto()
    {
        $this->load->model('Setting_model');   
        $data['gallery'] = $this->Setting_model->getgallery();
        $data['title'] = 'Gallery';

        $this->load->view('templates/booking_header',$data);
        $this->load->view('home/gallery',$data);
        $this->load->view('templates/booking_footer');
    }


    public function iklan(){

    }
}