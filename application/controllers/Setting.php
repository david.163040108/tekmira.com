<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Setting_model');
    }


    // ===================================== function gallery =============================================
    public function gallery(){
 
        $data['gallery'] = $this->Setting_model->getgallery();
        $data['title'] = 'Gallery';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('setting/gallery', $data);
        $this->load->view('templates/footer');
    }


    public function addGallery(){
        $data['title'] = 'Tambah Gallery';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('setting/addgallery', $data);
        $this->load->view('templates/footer');
    }

    public function addDbGallery(){
        $this->form_validation->set_rules('nama', 'Title', 'required|trim');
        $this->form_validation->set_rules('kategory', 'Kategory', 'required|trim');

         $nama = $this->input->post('nama');
         $kategory = $this->input->post('kategory');

     if ($this->form_validation->run() == false) {
         $this->addGallery();
     }else {
         $config['upload_path'] = './assets/img/gallery/';
         $config['allowed_types']        = 'gif|jpg|png';
         $config['max_size']             = '3048';

         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload())
         {
             $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');

             $this->addGallery();

         }else{

         $file = $this->upload->data();
         $gambar = $file['file_name'];

             $data = array(
                 'title' => $nama,
                 'category' => $kategory,
                 'img' => $gambar

             );	

             $status = $this->db->insert('tb_gallery', $data);

             if ($status) {
                     $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Simpan</div>');
             }else {
                     $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
             }

             redirect('setting/gallery');

         }
     }	
    }

    public function deleteGallery($id){
		$status = $this->Setting_model->deleteGallery($id);


		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Hapus</div>');
			 redirect('setting/gallery');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Hapus</div>');
            redirect('setting/gallery');
		}
    }

    //======================================= function promo =====================================================
    public function promo(){

        $data['promo'] = $this->Setting_model->getPromo();
        $data['title'] = 'Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('setting/promo', $data);
        $this->load->view('templates/footer');
    }

    public function addViewPromo(){
        $data['title'] = 'Tambah Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('setting/addpromo', $data);
        $this->load->view('templates/footer');
    }

    public function addDbPromo(){
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required|trim');

         $judul = $this->input->post('judul');
         $deskripsi = $this->input->post('deskripsi');

     if ($this->form_validation->run() == false) {
         $this->addViewPromo();
     }else {
         $config['upload_path'] = './assets/img/baner/';
         $config['allowed_types']        = 'gif|jpg|png';
         $config['max_size']             = '3048';

         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload())
         {
             $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');

             $this->addViewPromo();

         }else{

         $file = $this->upload->data();
         $gambar = $file['file_name'];

             $data = array(
                 'judul' => $judul,
                 'deskripsi' => $deskripsi,
                 'img' => $gambar

             );	

             $status = $this->db->insert('tb_promo_img', $data);

             if ($status) {
                     $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Simpan</div>');
             }else {
                     $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
             }

             redirect('setting/promo');

         }
     }	
    }

    public function deletePromo($id){
		$status = $this->Setting_model->deletePromo($id);


		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Hapus</div>');
			 redirect('setting/promo');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Hapus</div>');
            redirect('setting/promo');
		}
    }

}