<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kamar extends CI_Controller{
	public function __construct(){
        parent::__construct();
        is_logged_in();
        $this->load->model('Book_model');
		$this->load->model('Wisma_model');
		$this->load->model('Kamar_model');
    }

    public function index(){
    	$this->Kamar();
    }

	public function Kamar(){
		 $data['wisma'] = $this->Wisma_model->getWisma();

		 $search = $this->input->post('idwisma');
		 $data['id_wisma'] = $search;
		 //=================================================

		$data['title'] = 'Kamar';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Kamar_model');
		$data['kamar'] = $this->Kamar_model->getKamar($search);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/rooms', $data);
        $this->load->view('templates/footer');

	}

	//======================================================= Kamar =====================================================================


	public function editKamar($id){

		$data['title'] = 'Update Kamar';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Kamar_model');
        $this->load->model('Wisma_model');
        $data['wisma'] = $this->Wisma_model->getWisma();
        $data['type'] = $this->Kamar_model->getType();
        $data['detail'] =$this->Kamar_model->getdetailkamar($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/editrooms', $data);
        $this->load->view('templates/footer');

	}


	public function deleteKamar($id){
		$this->load->model('Kamar_model');
		$status = $this->Kamar_model->deleteKamar($id);


		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Hapus</div>');
			 redirect('kamar');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Hapus</div>');
			 redirect('kamar');
		}
	}


	public function addKamar(){

		$data['title'] = 'Tambah Kamar';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Kamar_model');
        $this->load->model('Wisma_model');
        $data['wisma'] = $this->Wisma_model->getWisma();
        $data['type'] = $this->Kamar_model->getType();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/addrooms', $data);
        $this->load->view('templates/footer');



	}


	public function updatekamardb(){

		$this->form_validation->set_rules('wisma', 'wisma', 'required|trim');
		$this->form_validation->set_rules('name', 'no_kamar', 'required|trim');
		$this->form_validation->set_rules('type', 'KelasKamar', 'required|trim');

		$type = $this->input->post('type');
		$name = $this->input->post('name');
		$wisma = $this->input->post('wisma');
		$id = $this->input->post('id');

		if ($this->form_validation->run() == false) {
			$this->editKamar($id);
		}else{
			$data = array(
				'id_type' => $type,
				'id_wisma' => $wisma,
				'no_kamar' => $name,
				'status' => 0,
				'id_kamar' => $id
			);

										$this->db->where('id_kamar', $id);
							$status = 	$this->db->update('tb_kamar', $data);

			if ($status) {
				 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Update</div>');
				 redirect('kamar');
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
				 redirect('kamar');
			}
		}

	}

	public function addKamardb(){

		$this->form_validation->set_rules('wisma', 'wisma', 'required|trim');
		$this->form_validation->set_rules('name', 'no_kamar', 'required|trim');
		$this->form_validation->set_rules('kelas', 'KelasKamar', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->addKamar();
		}else{
		$type = $this->input->post('kelas');
		$name = $this->input->post('name');
		$wisma = $this->input->post('wisma');

		$data = array(
			'id_type' => $type,
			'id_wisma' => $wisma,
			'no_kamar' => $name,
			'status' => 0
		);

		$status = $this->db->insert('tb_kamar', $data);

		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Tambah</div>');
			 redirect('kamar');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Tambah</div>');
			 redirect('kamar');
		}
		}
	}




	//================================ Kelas Kamar ========================================================================================
	public function KelasKamar(){
		$data['title'] = 'Kelas Kamar';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Kamar_model');
		$data['type'] = $this->Kamar_model->getType();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/type', $data);
        $this->load->view('templates/footer');
	}

	public function addType(){

		$data['title'] = 'Tambah Kelas';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/addtype', $data);
        $this->load->view('templates/footer');
	}

	public function add(){
		$this->form_validation->set_rules('type', 'type', 'required|trim');
		$this->form_validation->set_rules('weekend', 'Harga Weekend', 'required|trim');
		$this->form_validation->set_rules('weekday', 'Harga Weekday', 'required|trim');

				$type = $this->input->post('type');
				$weekend = $this->input->post('weekend');
				$weekday = $this->input->post('weekday');
				$des = $this->input->post('deskripsi');	

		if ($this->form_validation->run() == false) {
			$this->addType();
		}else{

				$config['upload_path'] = './assets/img/kamar/';
		        $config['allowed_types']        = 'gif|jpg|png';
		        $config['max_size']             = '2048';

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload())
		        {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');
					$this->addType();

		        }else{

			        $file = $this->upload->data();
			    	$gambar = $file['file_name'];

					$data = array(
						'type' => $type,
						'harga_weekday' => $weekday,
						'harga_weekend' => $weekend,
						'deskripsi' => $des,
						'img_type' => $gambar,
					);

					$status = 	$this->db->insert('tb_type_kamar', $data);

					if ($status) {
						 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil</div>');
						 redirect('kamar/KelasKamar');
					}else{
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal</div>');
						 redirect('kamar/KelasKamar');
					}
			
			}
		}
	}

	public function updateType($id){


		$data['title'] = 'Kelas Kamar';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Kamar_model');
		$data['type'] = $this->Kamar_model->getdetailtype($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('rooms/edittype', $data);
        $this->load->view('templates/footer');
	}

	public function updateTypedb(){
		$this->form_validation->set_rules('type', 'type', 'required|trim');
		$this->form_validation->set_rules('weekend', 'Harga Weekend', 'required|trim');
		$this->form_validation->set_rules('weekday', 'Harga Weekday', 'required|trim');

				$type = $this->input->post('type');
				$weekend = $this->input->post('weekend');
				$weekday = $this->input->post('weekday');
				$des = $this->input->post('deskripsi');
				$id = $this->input->post('id');		

		if ($this->form_validation->run() == false) {
			$this->updateType($id);
		}else{

			if($_FILES['userfile']['name'] != '')
			{

				$config['upload_path'] = './assets/img/kamar/';
		        $config['allowed_types']        = 'gif|jpg|png';
		        $config['max_size']             = '2048';

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload())
		        {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');
					$this->updateType($id);

		        }else{

			        $file = $this->upload->data();
			    	$gambar = $file['file_name'];

					$data = array(
						'type' => $type,
						'harga_weekday' => $weekday,
						'harga_weekend' => $weekend,
						'deskripsi' => $des,
						'img_type' => $gambar,
						'id_type'=> $id
					);

					$this->db->where('id_type', $id);
								$status = 	$this->db->update('tb_type_kamar', $data);

					if ($status) {
						 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Update</div>');
						 redirect('kamar/KelasKamar');
					}else{
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
						 redirect('kamar/KelasKamar');
					}
				}
			}else{
					$data = array(
						'type' => $type,
						'harga_weekday' => $weekday,
						'harga_weekend' => $weekend,
						'deskripsi' => $des,
						'id_type'=> $id
					);

					$this->db->where('id_type', $id);
								$status = 	$this->db->update('tb_type_kamar', $data);

					if ($status) {
						 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Update</div>');
						 redirect('kamar/KelasKamar');
					}else{
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
						 redirect('kamar/KelasKamar');
					}

			}
		}
	}

	public function deleteKelas($id){
	
		$status = $this->Kamar_model->deleteType($id);
		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Hapus</div>');
			 redirect('kamar/KelasKamar');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Hapus</div>');
			 redirect('kamar/KelasKamar');
		}
		
	}

	//========================================================Akhir Kelas Kamar=========================================================











	//================================================= Akhir 

}