<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wisma extends CI_Controller{
	public function __construct(){
        parent::__construct();
        is_logged_in();
        $this->load->model('Book_model');
    }

	public function index()
	{
        $data['notif'] = $this->Book_model->get_notif();
		
        $data['title'] = 'wisma';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Wisma_model');
        $data['wisma'] = $this->Wisma_model->getWisma();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('wisma/index', $data);
        $this->load->view('templates/footer');
    } 


    public function testing(){
        $data['title'] = 'wisma';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Wisma_model');
        $data['wisma'] = $this->Wisma_model->get_all();
       // $data['wisma1'] = $this->Wisma_model->getKamar1();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('wisma/test', $data);
        $this->load->view('templates/footer');
    }



    public function addWisma(){
         $data['notif'] = $this->Book_model->get_notif();

    	$data['title'] = 'Tambah Wisma';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();



        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('wisma/addWisma', $data);
        $this->load->view('templates/footer');

    } 

    public function updateWisma($id){
        $data['notif'] = $this->Book_model->get_notif();

        $data['title'] = 'Update Wisma';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Wisma_model');
        $data['wisma'] = $this->Wisma_model->getdetailwisma($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('wisma/updateWisma', $data);
        $this->load->view('templates/footer');

    }


    public function deleteWisma($id){

    	$this->load->model('Wisma_model');
		$status = $this->Wisma_model->deletewismadb($id);
        
		if ($status) {
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Hapus</div>');
			 redirect('wisma');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Hapus</div>');
			 redirect('wisma');
		}
    }

    public function addWismadb(){

    	       $this->form_validation->set_rules('nama', 'Name', 'required|trim');
    	       $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
    	       $this->form_validation->set_rules('telpone', 'Telpone', 'required|trim');


		    	$nama = $this->input->post('nama');
				$alamat = $this->input->post('alamat');
				$tlp = $this->input->post('telpone');

            if ($this->form_validation->run() == false) {
                $this->addWisma();
            }else {
				$config['upload_path'] = './assets/img/wisma/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '2048';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
        			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');

                	$this->addWisma();

                }else{

                $file = $this->upload->data();
            	$gambar = $file['file_name'];

					$data = array(
						'name' => $nama,
						'alamat' => $alamat,
						'no_telp' => $tlp,
						'image' => $gambar

					);	

					$status = $this->db->insert('tbl_wisma', $data);

					if ($status) {
							$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Simpan</div>');
					}else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
					}

					redirect('wisma');

                }
            }			

    }

    public function updateWismadb(){

        $this->form_validation->set_rules('nama', 'wisma', 'required|trim');
		$this->form_validation->set_rules('alamat', 'no_kamar', 'required|trim');
        $this->form_validation->set_rules('telpone', 'KelasKamar', 'required|trim');

        
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $tlp = $this->input->post('telpone');
        $id = $this->input->post('id');
        
        if ($this->form_validation->run() == false) {
            $this->updateWisma($id);
        }else {
            
            if($_FILES['userfile']['name'] != '')
            {
               
                $config['upload_path'] = './assets/img/wisma/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '2048';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Masukan Gambar</div>');

                    $this->updateWisma($id);

                }else{

                $file = $this->upload->data();
                $gambar = $file['file_name'];

                    $data = array(
                        'name' => $nama,
                        'alamat' => $alamat,
                        'no_telp' => $tlp,
                        'image' => $gambar

                    );  

                                                    $this->db->where('id_wisma', $id);
                        $status =   $this->db->update('tbl_wisma', $data);

                    if ($status) {
                            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Simpan</div>');
                    }else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
                    }

                    redirect('wisma');

                }   
            }else{

                    $data = array(
                        'name' => $nama,
                        'alamat' => $alamat,
                        'no_telp' => $tlp

                    );  

                                     $this->db->where('id_wisma', $id);
                        $status =   $this->db->update('tbl_wisma', $data);

                    if ($status) {
                            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil di Simpan</div>');
                    }else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gagal di Update</div>');
                    }

                    redirect('wisma');
            }
        }

    }



}

