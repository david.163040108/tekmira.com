<?php

class Laporan extends CI_Controller{


    public function __construct(){
        parent::__construct();
        is_logged_in();
        $this->load->model('Book_model');
        $this->load->model('Wisma_model');
    }

	public function laporan()
	{
        $search = $this->input->post('idwisma');
        $data['id_wisma'] = $search;
        $data['wisma'] = $this->Wisma_model->getWisma();        
        $d = $this->input->post('dari');
        $s = $this->input->post('sampai');
        if (!empty($d)) {
            $myinput1= $d; 
            $sqldate1=date('Y-m-d',strtotime($myinput1));
            $dari= $sqldate1;
             
        }else {
            $dari= "";
        }

        if (!empty($s)) {
            $myinput2= $s; 
            $sqldate2=date('Y-m-d',strtotime($myinput2));
            $sampai = $sqldate2; 
        }else {
            $sampai = "";
        }

        
        $data['chekout']= $this->Book_model->laporan($search,$dari,$sampai);
        $data['title'] = 'Laporan';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laporan/index', $data);
        $this->load->view('templates/footer');
        
    }  

    public function pdf(){
        $search = $this->input->post('idwisma');
		$data['id_wisma'] = $search;

		$data['wisma'] = $this->Wisma_model->getWisma();
        $d = $this->input->post('dari');
        $s = $this->input->post('sampai');
        if (!empty($d)) {
            $myinput1= $d; 
            $sqldate1=date('Y-m-d',strtotime($myinput1));
            $dari= $sqldate1;
            $data['dari'] =$dari;
             
        }else {
            $dari= "";
        }

        if (!empty($s)) {
            $myinput2= $s; 
            $sqldate2=date('Y-m-d',strtotime($myinput2));
            $sampai = $sqldate2; 
            $data['sampai'] =$sampai;
        }else {
            $sampai = "";
        }

        $data['chekout']= $this->Book_model->laporan($search,$dari,$sampai);

        $this->load->library('mypdf');
        $this->mypdf->generate('laporan/lpwisma',$data);
        
    }

    public function tes(){
        $id_wisma ="";
        $data['chekout']= $this->Book_model->laporan();
        $this->load->view('laporan/lpwisma',$data);
    }
}