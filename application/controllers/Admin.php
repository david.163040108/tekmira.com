<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Kamar_model');
        $this->load->model('Wisma_model');
        $this->load->model('Book_model');
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['pending'] = $this->Kamar_model->getbookingPending();
        $data['kosong'] = $this->Kamar_model->getkamarkosong();
        $data['booking'] = $this->Kamar_model->getkamarbooking();
        $data['notif'] = $this->Book_model->get_notif();
        $chart = $this->Book_model->chart();
        $chartline = $this->Book_model->chartline()->result();
        //json
        $data['chart'] = json_encode($chart);
        $data['tahun'] = json_encode($chartline);
        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }


    public function role()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get('user_role')->result_array();
        $data['notif'] = $this->Book_model->get_notif();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }


    public function roleAccess($role_id)
    {
        $data['title'] = 'Role Access';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();
        $data['notif'] = $this->Book_model->get_notif();
        
        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer');
    }


    public function changeAccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->num_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Changed!</div>');
    }


    public function kamarkosong(){
        $data['title'] = 'Detail Kamar Kosong';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['kosong'] = $this->Kamar_model->getberdasarkanwisma();
        $data['wisma'] = $this->Wisma_model->getWisma();
        $data['notif'] = $this->Book_model->get_notif();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/kamarksosong', $data);
        $this->load->view('templates/footer');
    }


    public function kamarterbooking(){
        $data['title'] = 'Detail Kamar Terbooking';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['wisma'] = $this->Wisma_model->getWisma();
        $data['kosong'] = $this->Kamar_model->getlistkamarbooking();
        $data['notif'] = $this->Book_model->get_notif();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/kamarterbooking', $data);
        $this->load->view('templates/footer');
    }



    public function kamarpending(){
        $data['title'] = 'Detail Kamar Pending';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['wisma'] = $this->Wisma_model->getWisma();
        $data['kosong'] = $this->Kamar_model->getlistkamarpending();
        $data['notif'] = $this->Book_model->get_notif();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/kamarpending', $data);
        $this->load->view('templates/footer');
    }


    public function notifikasi(){
       echo json_encode($this->Book_model->get_notif());
    }


    public function chart(){
        $data['title'] = 'Chart';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['wisma'] = $this->Wisma_model->getWisma();
        $search = $this->input->post('wisma');
        $data['id_wisma'] = $search;
        $search2 = $this->input->post('tahun');
        $data['tahun'] = $search2;
        
        $custom = $this->Book_model->customChart($search,$search2)->result();
        //json
        $data['custom'] = json_encode($custom);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/chart', $data);
        $this->load->view('templates/footer');
    }

}
