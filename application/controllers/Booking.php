<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        is_logged_in();
        $this->load->model('Book_model');
		$this->load->model('Kamar_model');
		$this->load->model('Wisma_model');


    }

// ===================================== tampil pemesanan baru =============================================
public function pemesananbaru(){

    //==================Model========================
    $data['notif'] = $this->Book_model->get_notif();
  
    //=================================================

    $data['title'] = 'Konfirmasi Pemesanan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $user =  $this->db->get_where('user', ['email' => $this->session->userdata('email')])->result();

    foreach ($user as $key ) {
      $admin_wisma = $key->name;
    }

    if ($admin_wisma == "tekmira1") {
    $data['konfirmasi'] = $this->Book_model->get_konfirmasi_wisma1();
              //=================View=======================
    $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/index', $data);
        $this->load->view('templates/footer');
        //===========================================
    }elseif ($admin_wisma == "tekmira2") {
		$data['konfirmasi'] = $this->Book_model->get_konfirmasi_wisma2();
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('booking/index', $data);
	  $this->load->view('templates/footer');
	  
    }elseif ($admin_wisma == "tekmira3") {
		$data['konfirmasi'] = $this->Book_model->get_konfirmasi_wisma3();
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('booking/index', $data);
      $this->load->view('templates/footer');
    }else {
      $data['konfirmasi'] = $this->Book_model->get_konfirmasi();
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('booking/index', $data);
      $this->load->view('templates/footer');
    }
  }
	//============================== pembatalan ========================================================
	public function pembatalan($id_type,$id_wisma,$jmlkmr,$id){
		        
		$status = $this->Book_model->pembatalanbook($id_type,$id_wisma,$jmlkmr,$id);
		if ($status) {
			 $this->session->set_flashdata('book', '<div class="alert alert-danger" role="alert">Gagal</div>');
			redirect('Booking/pemesananbaru');
		}else{
			$this->session->set_flashdata('book', '<div class="alert alert-success" role="alert">Berhasil</div>');
			redirect('Booking/pemesananbaru');
		}
	}

// ================================= get json ======================================================
	public function getdetail(){
	echo json_encode($this->Book_model->getdetailkonfirmasi($_POST['id']));
	}

	public function getdetilbook(){
		echo json_encode($this->Book_model->get_detailbook($_POST['id']));
	}

	public function getdetilchekout(){
		echo json_encode($this->Book_model->get_detailbook($_POST['id']));
	}	


// ==============================tampilan proses add booking ===================================================

	public function viewaddbook($id){

		//==================Model========================

		$book = $this->Book_model->getdetailkonfirmasi($id);
		
		$jml = $book->jumlah_kamar;
		$idwisma =  $book->id_wisma;

		$data['kamar'] = $this->Kamar_model->getkamarpending($jml,$idwisma);
		//================================================
		$data['kode_invoice'] = $book->kode_invoice;
		$data['name'] = $book->name;
		$data['id_wisma'] = $book->id_wisma;
		$data['id_type'] = $book->id_type;
		$data['type'] = $book->type;
		$data['nama'] = $book->nama;
		$data['telphone'] = $book->telphone;
		$data['email'] = $book->email;
		$data['tgl_masuk'] = $book->tgl_masuk;
		$data['tgl_keluar'] = $book->tgl_keluar;
		$data['jumlah_kamar'] = $book->jumlah_kamar;
		$data['total'] = $book->total;



		$data['title'] = 'Add Book';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        //=================View=======================
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/addbook', $data);
        $this->load->view('templates/footer');
        //===========================================

	}
// =============================== proses add booking ==================================

	public function addBook(){
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$nohp = $this->input->post('nohp');
			$kode = $this->input->post('kode');
			$wisma = $this->input->post('wisma');
			$type = $this->input->post('type');
			$tgl_masuk = $this->input->post('tgl_masuk');
			$tgl_keluar = $this->input->post('tgl_keluar');
			$jml_kmr = $this->input->post('jml_kmr');
			$total = $this->input->post('total');
			$kamar = $this->input->post('kamar', TRUE);

			$data = array(
				'nama' => $nama,
				'email' => $email,
				'no_hp' => $nohp,
				'kode_invoice' => $kode,
				'id_wisma' => $wisma,
				'id_type' => $type,
				'tgl_masuk' => $tgl_masuk,
				'tgl_keluar' => $tgl_keluar,
				'jml_kamar' => $jml_kmr,
				'total' => $total,
				'tgl_proses'=> date("Y-m-d H:i:s")

			);

			$this->Book_model->add_book($data,$kamar);
			//================================================


			$this->db->set('status', 1 );
			$this->db->where('kode_invoice', $kode);
			$status =$this->db->update('tabel_booking_konfirmasi');
			
		if ($status) {
			 $this->session->set_flashdata('book', '<div class="alert alert-success" role="alert">Berhasil di tambah</div>');
			redirect('Booking/pemesananbaru');
		}else{
			$this->session->set_flashdata('book', '<div class="alert alert-danger" role="alert">Gagal di tambah</div>');
			redirect('Booking/pemesananbaru');
		}
	}


// ====================================== tampil daftar booking ==========================================
	public function listbooking(){
	
		
		$data['book'] = $this->Book_model->getListBook();
		$user =  $this->db->get_where('user', ['email' => $this->session->userdata('email')])->result();

		foreach ($user as $key ) {
		  $admin_wisma = $key->name;
		}

		$data['title'] = 'List Booking';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        //=================View=======================
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		if ($admin_wisma == "tekmira1") {
			$data['book'] = $this->Book_model->getListBook_tekmira1();
			$this->load->view('booking/allbook', $data);
		}elseif ($admin_wisma == "tekmira2") {
			$data['book'] = $this->Book_model->getListBook_tekmira2();
			$this->load->view('booking/allbook', $data);
		}elseif ($admin_wisma == "tekmira3") {
			$data['book'] = $this->Book_model->getListBook_tekmira3();
			$this->load->view('booking/allbook', $data);
		}else {
			# code...
			$data['book'] = $this->Book_model->getListBook();
			$this->load->view('booking/allbook', $data);
		}
  
        $this->load->view('templates/footer');
        //===========================================
	}

//================================================================ chekout ============================================
	public function viewchekout(){

		$id		= $this->uri->segment(3);
		$data['chekout'] = $this->Book_model->get_detailbook($id);

		$data['title'] = 'chekout';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/proseschekout', $data);
        $this->load->view('templates/footer');

	}


	public function proseschekout(){
			$id = $this->input->post('id');
			$kamar = $this->input->post('kmr', TRUE);
			//====================================
			$this->db->trans_start();
				foreach($kamar AS $key){
				     $up= array(
				      'status'  	=> 0,
				     );

				     $this->db->where('id_kamar', $key);
				     $this->db->update('tb_kamar', $up);

			    }
				$this->db->trans_complete(); 

			$this->db->set('status', 1 );
			$this->db->where('id_book', $id);
			$status =$this->db->update('tb_book');

			//======================================
			
		if ($status) {
			 $this->session->set_flashdata('allbook', '<div class="alert alert-success" role="alert">Berhasil Chekout</div>');
			redirect('Booking/listbooking');
		}else{
			$this->session->set_flashdata('allbook', '<div class="alert alert-danger" role="alert">Gagal Chekout</div>');
			redirect('Booking/listbooking');
		}



	}
// ============================================================== proses perpanjang ========================================

	public function viewperpanjang(){


		$id		= $this->uri->segment(3);
		$id_type = $this->uri->segment(4);
		$data['perpanjang'] = $this->Book_model->get_detailbook($id);
		$data['harga'] = $this->Kamar_model->getdetailtype($id_type);

		$data['title'] = 'Perpanjang';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/perpanjang', $data);
        $this->load->view('templates/footer');
	}

//================================== proses perpanjang ================
	public function prosesperpanjang(){

			$id = $this->input->post('id');
			$tgl_keluar = $this->input->post('tglkeluar');
			$tglperpanjang = $this->input->post('tglperpanjang');
			$harga = $this->input->post('harga');
			$total = $this->input->post('total');
			$totalsebelumnya = $this->input->post('bayar');

			if ($total == "Salah") {
				$this->session->set_flashdata('allbook', '<div class="alert alert-danger" role="alert">Terjadi Kesalahan</div>');
				redirect('booking/listbooking');
			}else{
    $myinput1= $tglperpanjang; 
    $sqldate1=date('Y-m-d',strtotime($myinput1));   

    $myinput2= $tgl_keluar; 
    $sqldate2=date('Y-m-d',strtotime($myinput2)); 

			$sum = $total+$totalsebelumnya;

			$data1 = array(
				'id_book' => $id,
				'tanggal_keluar' => $sqldate2,
				'tgl_perpanjang' => $sqldate1,
				'harga_kamar' => $harga,
				'total' => $total,
				'tgl_proses' => date("Y-m-d H:i:s")
			);

			$data2 = array(
				'tgl_keluar' => $sqldate1,
				'total' => $sum
			);

		$this->Book_model->perpanjang($data1,$data2,$id);
			}
	}
// ========================================================================================================================

	public function listchekout(){
		
		$search = $this->input->post('idwisma');
		$data['id_wisma'] = $search;

		$data['wisma'] = $this->Wisma_model->getWisma();

		$data['chekout']= $this->Book_model->getListChekout($search);

		$data['title'] = 'Daftar Chekout';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/allchekout', $data);
        $this->load->view('templates/footer');
	}


	public function buktiperpanjangan($id){
		$invoice= $this->Book_model->getperpanjang($id);

		$data['title'] = 'Invoice';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		foreach ($invoice as $key ) {
			$data['invoice'] = $key->kode_invoice;
			$data['proses'] = $key->tgl_proses;
			$data['nama'] = $key->nama;
			$data['email'] = $key->email;
			$data['wisma'] = $key->name;
			$data['type'] = $key->type;
			$data['no'] = $key->no_hp;
			$data['tgl_perpanjang'] = $key->tgl_perpanjang;
			$data['harga'] = $key->harga_kamar;
			$data['kmr'] = $key->jml_kamar;
			$data['tgl_keluar'] = $key->tanggal_keluar;
			$data['total'] = $key->total;
			$data['alamat'] = $key->alamat;
			$data['no_telp'] = $key->no_telp;
			$keluar = $key->tanggal_keluar;;
			$perpanjang = $key->tgl_perpanjang;;
		}

		$k  = strtotime($keluar);
		$p= strtotime($perpanjang);
		$hasil       = $p - $k;
 		$data['sum'] = floor($hasil / (60 * 60 * 24));
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('booking/invoice', $data);
        $this->load->view('templates/footer');

	} 

	public function  feedbackbukti(){

	}

}