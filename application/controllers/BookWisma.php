<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BookWisma extends CI_Controller
{

        public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');

        $this->load->model('Wisma_model');
        $this->load->model('Kamar_model');
        $this->load->model('Book_model');
        $this->load->model('Kode_model');
        $this->load->model('Setting_model');
    }

	public function index()
	{

        $data['promo'] =$this->Setting_model->getPromo();
        $data['sum']   =$this->Setting_model->jumlahIklan();
        $data['wisma'] = $this->Wisma_model->getWisma();

        $this->load->view('templates/booking_header', $data);
        $this->load->view('wisma/wisma');
        $this->load->view('templates/booking_footer');
    }

    public function kelaskamar($id){

        $data['wisma'] = $this->Wisma_model->getdetailwisma($id);
        $data['type'] = $this->Wisma_model->get_all($id);

        $this->load->view('templates/booking_header', $data);
        $this->load->view('wisma/pilihkelas');
        $this->load->view('templates/booking_footer');
    }


    public function testing($id,$type){

        // 'trim|required|min_length[5]|max_length[12]

        
        $data['kode'] = $this->Kode_model->kode();
        $data['wisma'] = $this->Wisma_model->getdetailwisma($id);
        $data['type'] = $this->Kamar_model->getdetailtype($type);
        $data['total'] = $this->Book_model->get_all($id,$type);


        $this->load->view('templates/booking_header',$data);
        $this->load->view('booking/testing');
        $this->load->view('templates/booking_footer');
    }


    public function addBooking(){

        
        $this->form_validation->set_rules('tanggal_masuk', 'tanggal_masuk', 'required');
        $this->form_validation->set_rules('tanggal_keluar', 'tanggal_keluar', 'required');
        $this->form_validation->set_rules('jmlkmr', 'Jmlkmr', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('tlp', 'Telphone', 'required|trim|min_length[11]|max_length[12]|integer');

                $nama = $this->input->post('nama');
                $email = $this->input->post('email');
                $tlp = $this->input->post('tlp');
                $tgl_masuk = $this->input->post('tanggal_masuk');
                $tgl_keluar = $this->input->post('tanggal_keluar');
                $kode = $this->input->post('kode');
                $total = $this->input->post('total');
                $id_type = $this->input->post('id_type');
                $id_wisma = $this->input->post('id_wisma');
                $jmlkmr = $this->input->post('jmlkmr');
                $durasi = $this->input->post('durasi');
    
                $myinput1= $tgl_masuk; 
                $sqldate1=date('Y-m-d',strtotime($myinput1)); 
              
                $myinput2= $tgl_keluar; 
                $sqldate2=date('Y-m-d',strtotime($myinput2)); 
                if ($durasi == "Salah" || $durasi == "") {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Tanggal Chekin dan Chekout Salah</div>');
                    $this->testing($id_wisma,$id_type);
                }else {
                    
                
                if ($this->form_validation->run() == false) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Harap semua data di isi dengan benar!</div>');
                    // redirect('bookwisma/testing/'.$id_wisma.'/'.$id_type);
                    $this->testing($id_wisma,$id_type);
                }else {

                    $data = array(
                        'nama' => $nama,
                        'email' => $email,
                        'telphone' => $tlp,
                        'tgl_masuk' => $sqldate1,
                        'tgl_keluar' => $sqldate2,
                        'jumlah_kamar' => $jmlkmr,
                        'id_type' => $id_type,
                        'id_wisma' => $id_wisma,
                        'total' => $total,
                        'kode_invoice' => $kode,
                        'durasi' => $durasi,
                        'tanggal_proses' => date("Y-m-d H:i:s")
                    );

                        $status = $this->db->insert('tabel_booking_konfirmasi', $data);

                        $statuskamar = array('status' => 2);
                        $array = array('id_type' => $id_type, 'id_wisma' => $id_wisma, 'status' => 0);
                        $this->db->where($array);
                        $this->db->limit($jmlkmr);
                        $this->db->update('tb_kamar', $statuskamar);

                            if ($status) {
                            redirect('pembayaran/invoice/'.$kode);
                            }else {
                            redirect('bookwisma/testing');
                            }
                    }   
                }                  
    }



    private function _sendEmail()
    {
        //         $nama = $this->input->post('nama');
        //         $email = $this->input->post('email');
        //         $tlp = $this->input->post('tlp');
        //         $tgl_masuk = $this->input->post('tanggal_masuk');
        //         $tgl_keluar = $this->input->post('tanggal_keluar');
        //         $kode = $this->input->post('kode');
        //         $total = $this->input->post('total');
        //         $id_type = $this->input->post('id_type');
        //         $id_wisma = $this->input->post('id_wisma');
        //         $jmlkmr = $this->input->post('jmlkmr');
        //         $durasi = $this->input->post('durasi');
        // $config = [
        //     'protocol' => 'smtp',
        //     'smtp_host' => 'ssl://smtp.googlemail.com',
        //     'smtp_user' => 'officialscope01@gmail.com',
        //     'smtp_pass' => '!David0210',
        //     'smtp_port' => 465,
        //     'mailtype' => 'html',
        //     'charset' => 'utf-8',
        //     'newline' => "\r\n"
        // ];



        // $this->load->library('email', $config);
        // $this->email->initialize($config);
        // $this->email->from('officialscope01@gmail.com', 'Scope Official');
        // $this->email->to($this->input->post('email'));
        // $this->email->subject('Informasi Pembayaran');
        // $this->email->message($htmlcontent);
        // if ($this->email->send()){
        //     return true;
        // }else{
        //     echo $this->email->print_debugger();
        //     die;
        // }

    }

}