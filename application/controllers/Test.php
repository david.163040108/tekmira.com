<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{

	public function index()
	{

        $this->load->view('templates/booking_header');
        $this->load->view('wisma/test');
        $this->load->view('templates/booking_footer');
    }  
}