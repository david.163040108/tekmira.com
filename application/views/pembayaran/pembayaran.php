<?php 
	foreach ($invoice as $key ) {
		$nama = $key->nama;
		$wisma = $key->name;
		$alamat = $key->alamat;
		$no_telp = $key->no_telp;
		$no_hp = $key->telphone;
		$email = $key->email;
		$tgl_masuk = $key->tgl_masuk;
		$tgl_keluar = $key->tgl_keluar;
		$kode= $key->kode_invoice;
		$total = $key->total;
		$jmlkmr =$key->jumlah_kamar;
		$type = $key->type;
		$durasi = $key->durasi;
		$harga_weekend = $key->harga_weekend;
		$harga_weekday = $key->harga_weekday;
	}

 ?>

<div class="container">
		
		        <div class="text-left" style="margin-top: 50px;">
		            <button class="btn btn-info" onclick="printDiv('invoice')"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
		        </div>
	
<div class="row">
	<div class="col-md-8">
		<div id="invoice">
		    <div class="invoice overflow-auto">
		        <div>
		            <header>
		            	<hr>
		                <div class="row">
		                    <div class="col">
		                    	<h1>Tekimira.com</h1>
		                    </div>
		                    <div class="col company-details">
		                        <h4 class="name">
		                        	<?= $wisma ?>
		                        </h4>
		                        <div><?= $alamat ?></div>
		                        <div><?= $no_telp ?></div>
		                    </div>
		                </div>
		            </header>
		            <main>
		                <div class="row contacts">
		                    <div class="col invoice-to">
		                        <h4 class="to"><?= $nama  ?></h4>
		                        <div class="address"><?= $no_hp ?></div>
		                        <div class="email text-primary"><?= $email ?></div>
		                    </div>
		                    <div class="col invoice-details">
		                        <h4 class="invoice-id"><?= $kode ?></h4>
		                        <div class="date">Date of Invoice: <?= date('d/m/Y '); ?></div>
		                    </div>
		                </div>
		                <hr>
		                <div class="row">
			                <div class="col-md-8">
			                	<table>
			                		<tr >
			                			<td>Wisma</td>
			                			<td> :</td>
			                			<td><?= $wisma ?></td>
			                		</tr>
			                		<tr>
			                			<td>Tipe Kamar</td>
			                			<td> :</td>
			                			<td><?= $type ?></td>
			                		</tr>
			                		<tr>
			                			<td>Tanggal Masuk</td>
			                			<td> :</td>
			                			<td><?= $tgl_masuk ?></td>
			                		</tr>
			                		<tr>
			                			<td>Tanggal Keluar</td>
			                			<td> :</td>
			                			<td><?= $tgl_keluar ?></td>
			                		</tr>
			                	</table>
			                	
			                </div>               
			                <div class="col-md-4">
			                	<table>


			                		<tr>
			                			<td>Harga Sewa</td>
			                			<td>:</td>
			                			<td style="text-align: right;">
											<?php 	
											 $weekDay = date('w', strtotime($tgl_masuk ));											
											if($weekDay == 0 || $weekDay == 6) { 	?>
			                				Rp. <?= number_format($harga_weekend) ?>
			                				<?php } else{ ?>
			                				Rp. <?= number_format($harga_weekday) ?>
			                				<?php } ?>
			                			</td>
			                		</tr>
			                		<tr>
			                			<td>Jumlah Kamar</td>
			                			<td>:</td>
			                			<td style="text-align: right;"><?= $jmlkmr ?></td>
			                		</tr>	                		
			                		<tr>
			                			<td>Hari</td>
			                			<td>:</td>
			                			<td style="text-align: right;"><?= $durasi ?></td>
			                		</tr>
			                	</table>
			                </div>
		                </div>
		                <hr>                
		                <div class="row">
			                <div class="col-md-8">
			                	
			                </div>               
			                <div class="col-md-4">
			                	<table>


			                		<tr>
			                			<td>Grand Total</td>
			                			<td></td>
			                			<td></td>
			                			<td></td>
			                			<td style="text-align: right;">Rp. <?= number_format($total) ?></td>
			                		</tr>
			                	</table>
			                </div>
		                </div>

		                <br>
		                <h3>Terima Kasih</h3>
		                <div class="notices">
		                    <div>Pemberitahuan:</div>
		                    <div class="notice">Segera Lakukan Pembayaran sebelum 30 menit</div>
		                </div>
		            </main>
		        </div>
		        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
		        <div></div>
		    </div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="card border-dark mb-3">
			<div class="card-header">Cara Melakukan Pembayaran</div>
			<div class="card-body text-dark">
				<img style="width: 40%;" src="<?= base_url('assets/img/'); ?>bni_logo.svg" >
				<hr>
				<div class="row">
					<div class="col">
						<table class="table table-striped table-sm">
						<tr>
							<td>No Rekening</td>
							<td>0371989978</td>
						</tr>
						<tr>
							<td>Atas Nama</td>
							<td>Okta Rio</td>
						</tr>
						<tr>
							<td>Total</td>
							<td>Rp. <?= number_format($total) ?></td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			<ul>
				<li>Silahkan lakukan transfer sebesar Rp.<?= number_format($total) ?> </li>
				<li>Setalah melakukan pembayaran silahkan lakukan "Konfirmasi Pembayaran"</li>
				<li>Dengan Mengirimkan foto bukti pembayaran</li>
				<li>Konfirmasi bisa melalui Whatshapp</li>
				<li>No 08312391390</li>
			</ul>
		</div>
	</div>
	

</div>
</div>