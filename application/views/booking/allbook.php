<!-- Begin Page Content -->
<div class="container-fluid">
     <?= $this->session->flashdata('allbook'); ?>
        <div class="card shadow">
            <div class="card-header py-3">
                <span> <a href="#" class="btn btn-warning btn-circle btn-sm">
                    <i class="fas fa-exclamation-triangle"></i>
                  </a> Sudah Harus CekOut</span>
	            </div>
	            <div class="card-body">
	                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Kode</th>
	                      <th>Nama</th>
	                      <th>Wisma</th>
	                      <th>jumlah kamar</th>
	                      <th>Tgl_Masuk</th>
	                      <th>Tgl_Keluar</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </thead>
	                  <tfoot>
	                    <tr>
	                      <th>Kode</th>
	                      <th>Nama</th>
	                      <th>wisma</th>
	                      <th>Jumlah Kamar</th>
	                      <th>Tgl_Masuk</th>
	                      <th>Tgl_Keluar</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </tfoot>
	                  <tbody>
	                  	
	                <?php foreach($book as $td) : ?>
	                  	<?php if (strtotime(date('Y-m-d')) < strtotime($td->tgl_keluar)) { ?>
                             <tr>

                        <?php 
                        }else{ ?>
                           <tr class="bg-warning">
                        <?php } ?>
		                      <td><?= $td->kode_invoice ?></td>
		                      <td><?= $td->nama ?></td>
		                      <td><?= $td->name ?></td>
		                      <td><?= $td->jml_kamar ?></td>
		                      <td><?= $td->tgl_masuk?></td>
		                      <td><?= $td->tgl_keluar ?></td>
		                      <td class="text-center">
                                <a href="<?= site_url('booking/viewperpanjang/'.$td->id_book.'/'.$td->id_type.'')?>" class="btn btn-primary btn-circle btn-sm detail" >
                                  <i class="fas fa-key"></i>
                                </a> 

		                        <a href="" class="btn btn-success btn-circle btn-sm book" data-toggle="modal" data-target=".bd-example-modal-xl" data-id="<?= $td->id_book?>">
		                          <i class="fas fa-eye"></i>
		                        </a> 

		                        <a href="<?= site_url('booking/viewchekout/'.$td->id_book.'')?>" class="btn btn-danger btn-circle btn-sm">
		                          <i class="fas fa-door-open"></i>
		                        </a>
	
		                      </td>
		                    </tr>

                    <?php endforeach  ?>
	                  </tbody>
	                </table>
            </div>
        </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 
<!-- Extra large modal -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detail Konfirmasi Pemesanan</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-3">
        		<label>Kode Invoice</label>
        			<input type="text" class="form-control" id="kode" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Nama</label>
        		<input type="text" class="form-control" id="nama" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>No Hp</label>
        		<input type="text" class="form-control" id="nohp" readonly>
        		</div>
        		
        		<div class="col-md-3">
        		<label>Email</label>
        		<input type="text" class="form-control" id="email" readonly>
        		</div>	
        	</div>        	


        	<div class="row">
        		<div class="col-md-3">
        		<label>Tanggal Masuk</label>
        			<input type="text" class="form-control" id="tgl_masuk" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Tanggal Keluar</label>
        		<input type="text" class="form-control" id="tgl_keluar" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Jumlah Kamar</label>
        		<input type="text" class="form-control" id="jml_kamar" readonly>
				</div>
				
				<div class="col-md-3">
        		<label>Waktu</label>
        		<input type="text" class="form-control" id="waktu" readonly>
        		</div>
        		
        	</div>        	

        	<div class="row">
        		<div class="col-md-3">
        		<label>Tipe Kamar</label>
        			<input type="text" class="form-control" id="tipe" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Wimsa</label>
        		<input type="text" class="form-control" id="wisma" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Jumlah Bayar</label>
        		<input type="text" class="form-control" id="total" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>No Kamar</label>
        		<input type="text" class="form-control" id="no_kmr" readonly>
        		</div>

               

        	</div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
    </div>
  </div>
</div>

