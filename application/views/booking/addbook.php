
<!-- Begin Page Content -->
<div class="container-fluid text-dark" style="background-color: white">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
<form action="<?php echo site_url('Booking/addbook');?>" method="post">
    <div class="row">
                <div class="col-md-3">
                    <label>Kode Invoice</label>
                    <input type="text" class="form-control" id="nama" name="kode" readonly value="<?= $kode_invoice?>">
                </div>

               <div class="col-md-3">
                    <label>Wisma</label>
                    <input type="text" class="form-control" id="wisma" readonly value="<?= $name ?>" >
                    <input type="text" class="form-control" id="wisma" name="wisma" value="<?= $id_wisma ?>" hidden>
                </div>
                
                <div class="col-md-3">
                    <label>Tipe Kamar</label>
                    <input type="text" class="form-control" id="type" readonly value="<?= $type ?>">
                    <input type="text" class="form-control" id="type" name="type" value="<?= $id_type  ?>"  hidden>
                </div>
    </div>
    <hr>
    <div class="row">

        		<div class="col-md-3">
	        		<label>Nama</label>
	        		<input type="text" class="form-control" id="nama" name="nama" value="<?= $nama ?>"  readonly>
        		</div>

        		<div class="col-md-3">
	        		<label>No Hp</label>
	        		<input type="text" class="form-control" id="nohp" name="nohp" value="<?= $telphone  ?>" readonly>
        		</div>
        		
        		<div class="col-md-3">
	        		<label>Email</label>
	        		<input type="text" class="form-control" id="email" name="email" value="<?= $email  ?>" readonly>
        		</div>	   	
    </div>    
    <hr>
    <div class="row">

                <div class="col-md-3">
                    <label>Tanggal Masuk</label>
                    <input type="text" class="form-control" id="tgl_masuk" name="tgl_masuk" value="<?= $tgl_masuk ?>"  readonly>
                </div>

                <div class="col-md-3">
                    <label>Tanggal Keluar</label>
                    <input type="text" class="form-control" id="tgl_keluar" name="tgl_keluar" value="<?= $tgl_keluar?>" readonly>
                </div>
                
                <div class="col-md-3">
                    <label>Jumlah Kamar</label>
                    <input type="text" class="form-control" id="jml_kmr" name="jml_kmr" value="<?= $jumlah_kamar ?>" readonly>
                </div>

                <div class="col-md-3">
                    <label>Sudah Bayar</label>
                    <input type="text" class="form-control" id="total" name="total" value="<?= $total  ?>" readonly>
                </div>      
    </div>

    <div class="row" style="margin-top: 30px;">
    	<div class="col-md-10">
            <label>Pilih Kamar</label>
			<select class="selectpicker" name="kamar[]" data-live-search="true" data-width="50%" multiple data-style="btn-danger" required>
					              <?php 
					              	if (!empty($kamar)) {
                                        foreach ($kamar as $td)
                                        {
                                            ?>
                                            <?php if ($td->id_type == $id_type && $td->id_wisma == $id_wisma): ?>
                                            <option value="<?= $td->id_kamar ?>">
                                            
                                            		<?= $td->no_kamar ?> - <?= $td->type ?> - <?= $td->name  ?> 
                                            	

                                            </option>
                                            <?php endif ?>
                                            <?php
                                        }
					              	}
					               ?>
			</select>
    	</div>

    </div>
    <div class="row float-right " style="margin-right: 50px">
     <button type="submit" class="btn btn-success" onclick="return  confirm('Periksa Terlebih Dahulu Jika Sudah Tekan OK ')">simpan booking</button>
     </div>
</form>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 