  <?php foreach($total as $td) : ?>
  		<?php $total = $td->total ?>
  <?php endforeach  ?>

<!-- Begin Page Content -->
<div class="container">

	<!-- Page Heading -->

<br><br>
	
<?= $this->session->flashdata('message'); ?>
<h6>Tinjau Pesanan Anda</h6>
<div class="row">
	<div class="col-md-8 mb-5">
		<div class="card">
			<div class="card-body">
				<h6><?php echo $wisma->name; ?></h6>
				<hr>
				<div class="row">
					<div class="col-sm-4">
					<label>Tipe Kamar</label>
					<p><b><?php echo $type->type; ?></b></p>
					</div>

					<div class="col-sm-4">
					<label>Kapasitas</label>
					<p><b>2 Tamu</b></p>
					</div>
				</div>
				<hr>				
<?php echo form_open_multipart('bookwisma/addBooking');?>
				<div class="row">

					<div class="col-sm-4">
					<label> <span>Check in : <span id="tgl_awal"></span></span></label>
							<div class="input-group mb-3 date" >
								<input id="first" data-date-format="dd/mm/yyyy" class="form-control selector" name="tanggal_masuk" readonly required>
								<div class="input-group-append date">
								<span class="input-group-text" id="basic-addon2"><i class="fas fa-calendar-week"></i></span>
								</div>
							</div>
							 
					</div>

					<div class="col-sm-4">
					<label> <span>Check out : <span id="tgl_akhir"></span></span></label>
							<div class="input-group mb-3 date" >
								<input id="second" class="form-control second" name="tanggal_keluar" readonly required>
								<div class="input-group-append date">
								<span class="input-group-text" id="basic-addon2"><i class="fas fa-calendar-week"></i></span>
								</div>
							</div>
					</div>
					<div class="col-md-4">
					<label for="state">Jumlah Kamar</label>
					        	<select class="custom-select d-block w-100 kamar" id="kamar" name="jmlkmr" required>
					              <option value="">Kamar</option>

					              <?php for ($i=1; $i <= $total ; $i++) { ?>
					              	 <option value="<?= $i ?>"><?= $i ?></option>
					             <?php 					              		
					              } ?>
           
					            </select>

					</div>
  
					
			
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="card border-dark mb-3">
			<div class="card-header">Detail Harga</div>
			<div class="card-body text-dark">
				<p><?=$kode ?></p>

				<p>Harga per Malam <span class="float-right">IDR. <span id="harga"></span></span></p>
				<hr>
				<p>Malam <span id="jw"></span> x  <span id="jmlkmr"></span> Kamar <span  class="float-right" id="hs"></span></p>

				<hr>
				<b>Total Bayar : </b><span  class="float-right">IDR. <span id="total"></span></span>
		
			</div>
		</div>
	</div>
</div>
				 <input type="hidden" id="weekend"  value="<?= $type->harga_weekend ?>"> 
				 <input type="hidden" id="weekday"  value="<?= $type->harga_weekday ?>"> 
				 <input type="hidden" name="id_wisma"  value="<?php echo $wisma->id_wisma; ?>"> 
				 <input type="hidden" name="id_type" value="<?php echo $type->id_type; ?>">
				 <input type="hidden" name="kode" id="kode" value="<?=$kode ?>">
				 <input type="hidden" name="total" id="hasil">
				 <input type="hidden" name="durasi" id="durasi">


	<div class="row">
		<div class="col-md-8"> 
			<div class="card">
			<header class="card-header">
				<h6 class="card-title mt-2">Detail Pemesan</h6>
			</header>
			<article class="card-body">
				<div class="form-row">
					<div class="col form-group">
						<label>Nama</label>
						  <input type="text" class="form-control" placeholder="" name="nama">
						  <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email address</label>
					<input type="email" class="form-control" placeholder="" name="email">
					<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
				</div> <!-- form-group end.// -->

				<div class="form-row">
					<div class="col form-group">
					  <label>Nomor Telepon</label>
					  <input type="text" class="form-control" name="tlp">
					  <?= form_error('tlp', '<small class="text-danger pl-3">', '</small>'); ?>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->

			    <div class="form-group">
			        <button type="submit" class="btn btn-block text-white" style="background-color: #FF7200" > Lanjutkan Pembayaran </button>
			    </div> <!-- form-group// -->                                                
			</article> <!-- card-body end .// -->
			</div> <!-- card.// -->
</form>
	 	</div>
	 </div>
<br>

</div>

