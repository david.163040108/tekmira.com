<?php
//dikarenakan array multidimensi 
$kamar = "";
foreach ($perpanjang as $key ) {
	$id = $key->id_book;
	$nama = $key->nama;
	$kode = $key->kode_invoice;
	$nohp = $key->no_hp;
	$email = $key->email;
	$wisma = $key->name;
	$type = $key->type;
	$tglmasuk = $key->tgl_masuk;
	$tglkeluar = $key->tgl_keluar;
	$jmlkamar = $key->jml_kamar;
	$total = $key->total;
	$kamar .= $key->no_kamar;
	$sepasi = ", ";
	$kamar .= $sepasi; 
}


    $myinput1= $tglmasuk; 
    $sqldate1=date('m/d/Y',strtotime($myinput1)); 

    $myinput2= $tglkeluar; 
    $sqldate2=date('m/d/Y',strtotime($myinput2));
	 


 ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"></h1>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Proses Perpanjangan</h6>
            </div>
			    <div class="card-body">
			    	<form action="<?php echo site_url('Booking/prosesperpanjang');?>" method="post">
			    	<div class="row">
			    		<div class="col-md-6">
			    			<h6>Informasi Pengguna</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="kodeinvoice" class="col-sm-3 col-form-label">Kode Invoice</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="kode" value="<?= $kode  ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="nama" value="<?= $nama ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="nohp" class="col-sm-3 col-form-label">NoHp</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="nohp" value="<?= $nohp  ?>" readonly>
							    </div>
							  </div>

							 <div class="form-group row">
							    <label for="emial" class="col-sm-3 col-form-label">Email</label>
							    <div class="col-sm-9">
							  		<input type="text" class="form-control" id="email" value="<?= $email  ?>" readonly>
							    </div>
							  </div>
			    		</div>

			    		<div class="col-md-6">
			    			<h6>Informasi Booking</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="wisma" class="col-sm-3 col-form-label">Wisma</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="wisma" name="wisma" value="<?=$wisma  ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="type" class="col-sm-3 col-form-label">Type Kamar</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="wisma" name="type" value="<?= $type ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="tglmasuk" class="col-sm-3 col-form-label">Tanggal Masuk</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="tglmasuk" name="tglmasuk" value="<?= $sqldate1  ?>" readonly>
							    </div>
							  </div>

							 <div class="form-group row">
							    <label for="tglkeluar" class="col-sm-3 col-form-label">Tanggal Keluar</label>
							    <div class="col-sm-9">
							  		<input type="text" class="form-control" id="tglkeluar" name="tglkeluar" value="<?= $sqldate2 ?>" readonly>
							    </div>
							  </div>
			    		</div>

			    		<div class="col-md-6">
			    			<h6>Informasi Booking</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="jmlkamar" class="col-sm-3 col-form-label">Jumlah Kamar</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="jmlkamar" name="jmlkamar" value="<?= $jmlkamar ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="dftkmr" class="col-sm-3 col-form-label">Daftar Kamar</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="wisma" name="dftkmr" value="<?= $kamar  ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="bayar" class="col-sm-3 col-form-label">Jumlah Bayar</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="tglmasuk" name="bayar"  value="<?= $total  ?>" readonly>
							    </div>
							  </div>
			    		</div>
			    		<div class="col-md-6">
			    			<h6>Informasi Perpanjangan {Harga Kamar Hari Ini}</h6>
			    			<hr>
			    			<input type="hidden" name="id" value="<?= $id  ?>">
			    			<input type="hidden" id="total" name="total" >
			    			<input type="hidden" id="total" name="tglkeluar" value="<?= $tglkeluar ?>" >


							  <div class="form-group row">
							    <label for="dftkmr" class="col-sm-3 col-form-label">Tanggal Keluar</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control datepicker selector" name="tglperpanjang" id="datepicker" required>
							    </div>
							  </div>

							  <div class="form-group row">
							    <label for="jmlkamar" class="col-sm-3 col-form-label">Harga Kamar</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control"  id="harga" name="harga" readonly>
							     	<input type="hidden" class="form-control"  id="weekday" value="<?= $harga->harga_weekday ?>" >
							     	<input type="hidden" class="form-control"  id="weekend" value="<?= $harga->harga_weekend ?>">
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="bayar" class="col-sm-3 col-form-label">Harus Bayar</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control byr" id="hasil"    readonly>
							    </div>
							  </div>
							<div><button class="btn btn-primary" type="submit" onclick="return  confirm('Periksa Terlebih Dahulu Y/N')">Proses</button></div>	
			    		</div>			  			  		
			    	</div>
			    </form>
			    </div>
		</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 