<?php 

//multidimensi array dikarenakan
$idkmr = array();
$kamar = "";
foreach ($chekout as $key ) {
	$id = $key->id_book;
	$nama = $key->nama;
	$kode = $key->kode_invoice;
	$nohp = $key->no_hp;
	$email = $key->email;
	$wisma = $key->name;
	$type = $key->type;
	$tglmasuk = $key->tgl_masuk;
	$tglkeluar = $key->tgl_keluar;
	$jmlkamar = $key->jml_kamar;
	$total = $key->total;
	$kamar .= $key->no_kamar;
	$sepasi = ", ";
	$kamar .= $sepasi;

	array_push($idkmr, $key->id_kamar); 
}

 ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"></h1>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Proses Chekout</h6>
            </div>
			    <div class="card-body">
			    	<div class="row">
			    		<div class="col-md-6">
			    			<h6>Informasi Pengguna</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="kodeinvoice" class="col-sm-3 col-form-label">Kode Invoice</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="kode" value="<?= $kode  ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="nama" value="<?= $nama ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="nohp" class="col-sm-3 col-form-label">NoHp</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="nohp" value="<?= $nohp  ?>" readonly>
							    </div>
							  </div>

							 <div class="form-group row">
							    <label for="emial" class="col-sm-3 col-form-label">Email</label>
							    <div class="col-sm-9">
							  		<input type="text" class="form-control" id="email" value="<?= $email  ?>" readonly>
							    </div>
							  </div>
			    		</div>

			    		<div class="col-md-6">
			    			<h6>Informasi Booking</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="wisma" class="col-sm-3 col-form-label">Wisma</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="wisma" name="wisma" value="<?=$wisma  ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="type" class="col-sm-3 col-form-label">Type Kamar</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="wisma" name="type" value="<?= $type ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="tglmasuk" class="col-sm-3 col-form-label">Tanggal Masuk</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="tglmasuk" name="tglmasuk" value="<?= $tglmasuk  ?>" readonly>
							    </div>
							  </div>

							 <div class="form-group row">
							    <label for="tglkeluar" class="col-sm-3 col-form-label">Tanggal Keluar</label>
							    <div class="col-sm-9">
							  		<input type="text" class="form-control" id="tglkeluar" name="tglkeluar" value="<?= $tglkeluar ?>" readonly>
							    </div>
							  </div>
			    		</div>

			    		<div class="col-md-6">
			    			<h6>Informasi Booking</h6>
			    			<hr>
							  <div class="form-group row">
							    <label for="jmlkamar" class="col-sm-3 col-form-label">Jumlah Kamar</label>
							    <div class="col-sm-9">
							     	<input type="text" class="form-control" id="wisma" name="jmlkamar" value="<?= $jmlkamar ?>" readonly>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="dftkmr" class="col-sm-3 col-form-label">Daftar Kamar</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="wisma" name="dftkmr" value="<?= $kamar  ?>" readonly>
							    </div>
							  </div>
							 
							 <div class="form-group row">
							    <label for="bayar" class="col-sm-3 col-form-label">Jumlah Bayar</label>
							    <div class="col-sm-9">
							  		 <input type="text" class="form-control" id="tglmasuk" name="bayar" value="<?= $total  ?>" readonly>
							    </div>
							  </div>
			    		</div>	

			  			<div class="col-md-6">
			  				<div class="mt-5">
			  				<form action="<?php echo site_url('Booking/proseschekout');?>" method="post">
			  			 	<input type="hidden" name="id" id="id" value="<?= $id ?>"> 

			  			 	<?php foreach ($idkmr as $key) { ?>
			  			 		<input type="hidden" name="kmr[]" id="id" value="<?= $key ?>"> 	
			  			 	<?php } ?>
			  			 	
			  					<button class="btn btn-primary" type="submit" onclick="return  confirm('Periksa Terlebih dahulu, Jika Sudah Tekan Ok')">Proses Chekout</button>
			  				</form> 	
			  				</div>
			  			</div>	
			  			   		
			    	</div>
			    </div>
		</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 