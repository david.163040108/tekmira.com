
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="col-md-12 text-center mb-3 mt-5 hilang">
        <button class="btn btn-primary" id="printInvoice">Print</button>
        <button class="btn btn-danger">Kembali</button>
    </div>
<div class="invoice">
    <div>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="#" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                <?= $invoice ?>  <br>
                                <?= $proses ?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                            <?= $wisma ?><br>
                            <div class="row">
                            <div class="col-md-8">
                            <?= $alamat ?>
                            </div>
                            </div>
                            <?= $no_telp ?>
                            </td>
                            
                            <td>
                            <?= $nama ?><br>
                            <?= $no ?><br>
                            <?= $email ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Detail
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="item">
                <td>
                Wisma 
                </td>
                
                <td>
                <?= $wisma ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    Tipe Kamar
                </td>
                
                <td>
                <?= $type ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    Tanggal Keluar
                </td>
                
                <td>
                <?= $tgl_keluar ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    Tanggal Perpanjang
                </td>
                
                <td>
                <?= $tgl_perpanjang ?>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price
                </td>
            </tr>

            
            <tr class="item">
                <td>
                    Harga Sewa Kamar
                </td>
                
                <td>
                    Rp. <?= number_format($harga) ?>
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Jumlah Kamar
                </td>
                
                <td>
                <?= $kmr ?>
                </td>
            </tr>
            
            <tr class="item last">
                <td>
                    Lama Menginap
                </td>
                
                <td>
                <?= $sum ?>
                </td>
            </tr>
            
            <tr class="total">
                <td></td>
                
                <td>
                Rp. <?= number_format($total) ?>
                </td>
            </tr>
        </table>
    </div>
    </div>
</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 