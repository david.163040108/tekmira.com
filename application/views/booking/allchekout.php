<?php
$selected = "selected='selected'";
?>
<!-- Begin Page Content -->
<div class="container-fluid">
 
        <div class="card shadow">
            <div class="card-header py-3">
			<div class="row">
                <div class="col">
					Daftar Chekout
                </div>
                <div class="col-md-4">

                <form  action="<?= base_url('booking/listchekout') ?>" method="post" id="">
                  <div class="row">
                    <div class="col">
                      <select class="form-control" id="exampleFormControlSelect1" name="idwisma">
                        <option value="">Semua Wisma</option>
                        <?php
                          if(!empty($wisma))
                          {
                              foreach ($wisma as $td)
                              {
                                  ?>
                                  <option value="<?php echo $td->id_wisma ?>"
                                    <?php if($td->id_wisma == $id_wisma) { echo $selected; } ?>><?php echo $td->name ?></option>
                                  <?php
                              }
                          }
                        ?>
                      </select>
                    </div>                    
                      <div class="col-md-0">
                          <button class="btn btn-primary" type="submit">ok</button>
                      </div>
                  </div>
                </form>
                </div>
              </div>
	            </div>
	            <div class="card-body">
	                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Kode</th>
	                      <th>Nama</th>
	                      <th>Wisma</th>
	                      <th>jml kmr</th>
	                      <th>Tgl_Masuk</th>
	                      <th>Tgl_Keluar</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </thead>
	                  <tfoot>
	                    <tr>
	                      <th>Kode</th>
	                      <th>Nama</th>
	                      <th>wisma</th>
	                      <th>Jml kmr</th>
	                      <th>Tgl_Masuk</th>
	                      <th>Tgl_Keluar</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </tfoot>
	                  <tbody>
                        <?php foreach($chekout as $key) : ?>
                             <tr>
		                      <td><?= $key->kode_invoice; ?></td>
                              <td><?= $key->nama ?></td>
                              <td><?= $key->name ?></td>
                              <td><?= $key->jml_kamar ?></td>
                              <td><?= $key->tgl_masuk ?></td>
                              <td><?= $key->tgl_keluar ?></td>

		                      <td class="text-center">
                                <a href="" class="btn btn-success btn-circle btn-sm book2" data-toggle="modal" data-target=".bd-example-modal-xl" data-id="<?= $key->id_book?>">
                                  <i class="fas fa-eye"></i>
                                </a> 
		                      </td>
		                    </tr>
                         <?php endforeach  ?>
	                  </tbody>
	                </table>
            </div>
        </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 
<!-- Extra large modal -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detail Konfirmasi Pemesanan</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-3">
        		<label>Kode Invoice</label>
        			<input type="text" class="form-control" id="kode" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Nama</label>
        		<input type="text" class="form-control" id="nama" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>No Hp</label>
        		<input type="text" class="form-control" id="nohp" readonly>
        		</div>
        		
        		<div class="col-md-3">
        		<label>Email</label>
        		<input type="text" class="form-control" id="email" readonly>
        		</div>	
        	</div>        	


        	<div class="row">
        		<div class="col-md-3">
        		<label>Tanggal Masuk</label>
        			<input type="text" class="form-control" id="tgl_masuk" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Tanggal Keluar</label>
        		<input type="text" class="form-control" id="tgl_keluar" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Jumlah Kamar</label>
        		<input type="text" class="form-control" id="jml_kamar" readonly>
        		</div>
        		
        	</div>        	

        	<div class="row">
        		<div class="col-md-3">
        		<label>Tipe Kamar</label>
        			<input type="text" class="form-control" id="tipe" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Wimsa</label>
        		<input type="text" class="form-control" id="wisma" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>Jumlah Bayar</label>
        		<input type="text" class="form-control" id="total" readonly>
        		</div>

        		<div class="col-md-3">
        		<label>No Kamar</label>
        		<input type="text" class="form-control" id="no_kmr" readonly>
        		</div>

               

        	</div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
    </div>
  </div>
</div>

