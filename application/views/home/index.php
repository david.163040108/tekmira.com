     
     <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="intro-info">
                <h6 class="display-4 animated bounceInRight">One Solution <span>for</span><br>many <span>things</span> with us </h6> 
              </div>
            </div>
            <div class="col-md-6">
              <div class="intro-img">
                <img src="<?= base_url('assets/'); ?>img/book.svg" alt="" class="img-fluid">
              </div>
            </div>
        </div>
      </div>
    </div>
    <!-- akhir Jumbotron -->

    <!-- container -->
    <div class="container">

      <!-- info panel -->
      <div class="row justify-content-center info">
        <div class="col-10 info-panel">
          <div class="row">
            <div class="col-lg">
              <img src="<?= base_url('assets/'); ?>img/wisma.svg" alt="Employee" class="img-fluid float-left">
              <h4>Wisma</h4>
              <p>Booking Lebih Mudah. Nyaman dan Aman</p>
            </div>
            <div class="col-lg">
              <img src="<?= base_url('assets/'); ?>img/room.svg" alt="HiRes" class="img-fluid float-left">
              <h4> Meeting Room</h4>
              <p>Booking Lebih Mudah. Nyaman dan Aman</p>
            </div>
            <div class="col-lg">
              <img src="<?= base_url('assets/'); ?>img/hall.svg" alt="Security" class="img-fluid float-left">
              <h4> Hall</h4>
              <p>Booking Lebih Mudah. Nyaman dan Aman</p>
            </div>
          </div>
        </div>
      </div>
      <!-- akhir info panel -->


      <!-- Workingspace -->
      <div class="row workingspace workingspace2">
         <div class="col-lg-1"></div>
        <div class="col-lg-5">
          <img style="border-radius: 15px; max-height: 100%;" src="<?= base_url('assets/'); ?>img/wismabg/wisma2.jpg" alt="Working Space" class="img-fluid">
        </div>
        <div class="col-lg-5 animated bounceInRight">
          <h2>Booking <span>Kamar</span> Lebih <span>Mudah</span></h2>
          <p>Sekarang Booking kamar wisma lebih mudah, Liat Lebih lengkap dibawah.</p>
          <a href="<?= base_url('home/galleryphoto'); ?>" class="btn btn-success tombol">Gallery</a>
          <a class="venobox btn btn-danger tombol2" data-autoplay="true" data-vbtype="video" href="https://www.youtube.com/watch?v=1ak17RxcqBQ">Youtube</a>
        </div>
      </div>
      <!-- akhir Workingspace -->

      <!-- Workingspace 2-->
      <div class="row workingspace">
        <div class="col-lg-1"></div>
        <div class="col-lg-5 line2">
          <h2>Sewa <span>Gedung</span> Lebih <span>Mudah</span></h2>
          <p>Sekarang Sewa Gedung lebih mudah ,liat lebih lengkap dibawah.</p>
          <a href="<?= base_url('home/galleryphoto'); ?>" class="btn btn-success tombol">Gallery</a>
          <a class="venobox btn btn-danger tombol2" data-autoplay="true" data-vbtype="video" href="http://youtu.be/xxx">Youtube</a>
        </div>
        <div class="col-lg-5">
          <img style="border-radius: 15px; max-height: 100%;" src="<?= base_url('assets/'); ?>img/hall.jpg" alt="Working Space" class="img-fluid">
        </div>
      </div>
      <!-- akhir Workingspace 2 -->


      <!-- Workingspace3 -->
      <div class="row workingspace workingspace2">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
          <img style="border-radius: 15px; max-height: 100%;" src="<?= base_url('assets/'); ?>img/room.jpg" alt="Working Space" class="img-fluid">
        </div>
        <div class="col-lg-5">
          <h2>Booking <span>Ruang</span> Rapat lebih <span>Mudah</span></h2>
          <p>Sekarang Booking Ruang Rapat lebih mudah, liat lebih lengkap dibawah.</p>
          <a href="<?= base_url('home/galleryphoto'); ?>" class="btn btn-success tombol">Gallery</a>
          <a class="venobox btn btn-danger tombol2" data-autoplay="true" data-vbtype="video" href="http://youtu.be/xxx">Youtube</a>
        </div>
      </div>
      <!-- akhir Workingspace -->



      <!-- Testimonial -->
      <section class="testimonial">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <p>"One Solution for many things with us."</p>
          </div>
        </div>
      </section>
      <!-- akhir Testimonial -->

      <br>
      <br>
      <br>

    </div>
    <!-- akhir container -->


