  <div>
  <section id="gallery">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Our gallery</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="gallery-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-wisma">Wisma</li>
              <li data-filter=".filter-room">Room</li>
              <li data-filter=".filter-hall">Hall</li>
            </ul>
          </div>
        </div>

        <div class="row gallery-container">

        <?php if (empty($gallery)) { ?>
          <H1>Gallery Kosong</H1>
        <?php }else{  ?>
        <?php foreach($gallery as $td) : ?>
          <div class="col-lg-4 col-md-6 gallery-item filter-<?= $td->category ?>">
            <div class="gallery-wrap">
              <img src="<?= base_url('assets/'); ?>img/gallery/<?=$td->img?>" class="img-fluid" alt="">
              <div class="gallery-info">
                <h4><a href="#"><?= $td->title  ?></a></h4>
                <p><?= $td->category ?></p>
                <div>
                  <a href="<?= base_url('assets/'); ?>img/gallery/<?=$td->img?>" data-lightbox="gallery" data-title="<?= $td->title  ?>" class="link-preview" title="Preview"><i class="fas fa-eye"></i></i></a>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach  ?>
        </div>
        <?php }  ?>
      </div>
    </section><!-- #gallery -->
