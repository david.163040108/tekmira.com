<main role="main">


<?php if (!empty($promo)) { ?>

<div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php for ($x = 0; $x < $sum; $x++) { ?>
          <li data-target="#carouselExampleCaptions" data-slide-to="<?= $x ?>" 
          <?php if ($x == 0) {?>
            class="active"
          <?php }  ?>></li>
        <?php  } ?>
    </ol>

    <div class="carousel-inner">

        <?php $i=0; foreach($promo as $td) : ?>
          <?php $sum = $i++; if ($sum == 0) { ?>
        <div class="carousel-item active">
          <?php }else{  ?>
        <div class="carousel-item">
        <?php }  ?>

          <img src="<?= base_url('assets/'); ?>img/baner/<?= $td->img ?>" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5><?= $td->judul ?></h5>
            <p><?= $td->deskripsi ?></p>
        </div>
      </div>
      <?php endforeach  ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<?php } ?>   

  <div class="album py-5 bg-light">
    <div class="container">


      <div class="row">
      <?php foreach($wisma as $td) : ?>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img width="100%" height="225" src="<?=base_url('assets/img/wisma/') ?><?= $td->image ?>">
            <div class="card-body">
              <h5><?=$td->name ?></h5>
              <p class="card-text"><?= $td->alamat  ?></p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                
                  <a href="<?= site_url('BookWisma/kelaskamar/'.$td->id_wisma.'')?>" class="btn btn-sm btn-primary tombol text-white">Pesan Kamar</a>
                </div>
              
              </div>
            </div>
          </div>
        </div>
        <?php endforeach  ?>


      </div>
    </div>
  </div>

</main>


