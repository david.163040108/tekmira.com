<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <?= $this->session->flashdata('message'); ?>



        	<br>
    	<br>
         <div class="card shadow">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Kelas Kamar</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No Kamar</th>
                      <th>Type</th>
                      <th>Wisma</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No Kamar</th>
                      <th>Type</th>
                      <th>Wisma</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php foreach($wisma as $td) : ?>
                    <tr>
                      <td>a</td>
                      <td><?= $td->type ?></td>
                      <td><?= $td->total ?></td>
                      <td class="text-center">s</td>


                      <td class="text-center">


                        <a href="#" class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target="#deleteModal">
                          <i class="fas fa-trash"></i>
                        </a>

                      </td>
                    </tr>
                     <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>



</div>
<!-- /.container-fluid -->
</div>


