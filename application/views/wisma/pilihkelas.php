<!-- Page Content -->
<div class="container container-fluid" style="margin-top: 50px;">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4">
    <?= $wisma->name ?>
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-7">
      <img class="img-fluid rounded" src="<?=base_url('assets/img/wisma/') ?><?= $wisma->image ?>" style="width: 600px; height: 350px;" alt="">
    </div>

    <div class="col-md-4">
      <h5 class="my-3">Alamat</h5>
      <p><?= $wisma->alamat  ?></p>
      <h5 class="my-3">Telphone</h5>
      <ul>
        <li><?= $wisma->no_telp ?></li>
      </ul>
    </div>
  </div>
  <hr>

  <h3 class="my-4">Daftar Kelas yang Tersedia</h3>
  <?php if (!empty($type)): ?>
    
  
  <?php foreach($type as $td) : ?>
      <div class="row">
        <div class="col-md-5">
          <a href="#">
            <img class="img-fluid rounded mb-3 mb-md-0 rounded" src="<?=base_url('assets/img/kamar/') ?><?= $td->img_type ?>" alt="" >
          </a>
        </div>
        <div class="col-md-5">
         <h3><?= $td->type ?></h3>

          <p>WeekEnd <span style="color:  #FF7200;"><b>Rp.<?= number_format($td->harga_weekend) ?></b></span> ||
          WeekDay <span style="color:  #FF7200;"><b>Rp.<?= number_format($td->harga_weekday) ?></b></span>
          </p>

          <span><?= $td->deskripsi ?></span>
          <p class="text-danger"><?= $td->total ?> Kamar Tersisa</p>
          <a class="btn text-white" style="background-color: #FF7200" href="<?= site_url('bookwisma/testing/'.$wisma->id_wisma.'/'.$td->id_type)?>">Pesan</a>
        </div>
      </div>

      <hr>
  <?php endforeach  ?>


  <?php else: ?>
    <div class="col-md-12">
      <h4 class="text-center">Mohon Maaf Kamar Sudah Habis</h4>
    </div>
    <br>
    <br>
  <?php endif ?>
 
</div>
<!-- /.container -->