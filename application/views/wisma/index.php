<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
	
	<?= $this->session->flashdata('message'); ?>
    
    <a href="<?= base_url('wisma/addwisma'); ?>" class="btn btn-primary btn-icon-split">
     	<span class="icon text-white-50">
        	<i class="fas fa-file"></i>
        </span>
     			<span class="text">Tambah Data</span>
    </a>

            	<br>
    	<br>
         <div class="card shadow">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Kelas Kamar</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Gambar</th>
                      <th>Wisma</th>
                      <th>Alamat</th>
                      <th>Telephone</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Gambar</th>
                      <th>Wisma</th>
                      <th>Alamat</th>
                      <th>Telephone</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  	   <?php foreach($wisma as $td) : ?>
                    <tr>
                      <td><img style="height: 50px; width: 50px;" src="<?=base_url('assets/img/wisma/') ?><?= $td->image ?>"></td>
                      <td><?= $td->name  ?></td>
                      <td><?= $td->alamat ?></td>
                      <td><?= $td->no_telp ?></td>
                      <td class="text-center">
                        <a href="<?= site_url('wisma/updatewisma/'.$td->id_wisma.'')?>" class="btn btn-success btn-circle btn-sm">
                          <i class="fas fa-edit"></i>
                        </a> 

                        <a href="#" class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target="#deleteModal">
                          <i class="fas fa-trash"></i>
                        </a>

                      </td>
                    </tr>
                    <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>


</div>
<!-- /.container-fluid -->


  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Apakah Anda Yakin Ingin Menghapus</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= site_url('wisma/deletewisma/'.$td->id_wisma.'')?>">delete</a>
        </div>
      </div>
    </div>
  </div>

