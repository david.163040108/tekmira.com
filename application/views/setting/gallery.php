<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
	
	<?= $this->session->flashdata('message'); ?>
    
    <a href="<?= base_url('setting/addgallery'); ?>" class="btn btn-primary btn-icon-split">
     	<span class="icon text-white-50">
        	<i class="fas fa-file"></i>
        </span>
     			<span class="text">Tambah Data</span>
    </a>

            	<br>
    	<br>
         <div class="card shadow">
            <div class="card-header py-3">
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Images</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Images</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  	   <?php foreach($gallery as $td) : ?>
                    <tr>
                      <td><img style="height: 50px; width: 50px;" src="<?=base_url('assets/img/gallery/') ?><?= $td->img ?>"></td>
                      <td><?= $td->title  ?></td>
                      <td><?= $td->category ?></td>
                      <td class="text-center">
                      <a href="<?= site_url('setting/deletegallery/'.$td->id.'')?>" class="btn btn-danger btn-circle btn-sm" onclick="return  confirm('do you want to delete Y/N')">
                          <i class="fas fa-trash"></i>
                      </a>

                      </td>
                    </tr>
                    <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>


</div>
<!-- /.container-fluid -->


