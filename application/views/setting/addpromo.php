<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <?= $this->session->flashdata('message'); ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
            </div>
			    <div class="card-body">
			    	<?php echo form_open_multipart('setting/adddbpromo');?>
				        <div class="row">
				          <div class="col-md-4 mb-3">
				            <label for="country">Judul</label>
				           	<input type="text" class="form-control"  name="judul">
				           	 <?= form_error('judul', '<small class="text-danger pl-3">', '</small>'); ?>
                          </div>
                            
                            <div class="col-sm-4">
						        	<label for="country">Gambar</label>
				                    <div class="custom-file">
				                        <input type="file" class="custom-file-input" id="image" name="userfile">
				                        <label class="custom-file-label" for="image">Choose file</label>
				                    </div>
				            </div>
				        </div>
				        <div class="row">
                            <div class="col-md-8">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea id="deskripsi" style="width: 100%" name="deskripsi"></textarea>
                                <?= form_error('deskripsi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        
				    	<hr class="mb-4">
				    	<div class="row">
				    		<div class="col-md-4">
				    			<button class="btn btn-primary" type="submit">Submit </button>
							</div>
						</div>
					</form>
			    </div>
		</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 

