<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>

            <a href="<?= base_url('setting/addviewpromo'); ?>" class="btn btn-primary mb-3">Tambah Promo</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($promo as $r) : ?>
                    <tr>
                        <th scope="row"><?= $i; ?></th>
                        <td><?= $r->judul; ?></td>
                        <td><?= $r->deskripsi; ?></td>
                        <td><?= $r->img; ?></td>
                        <td>
                        <a href="<?= site_url('setting/deletepromo/'.$r->id.'')?>" class="btn btn-danger btn-circle btn-sm" onclick="return  confirm('do you want to delete Y/N')">
                          <i class="fas fa-trash"></i>
                        </a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

