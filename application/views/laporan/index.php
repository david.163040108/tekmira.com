<?php
$selected = "selected='selected'";
?>
<div class="container-fluid">

<form action="<?= base_url('laporan/laporan') ?>" method="POST">
<div class="row mb-3">

   <div class="col-md-3">
      <label for="dari">Dari Tanggal</label>
      <input type="date"  class="form-control" name="dari" id="dari"></div>
   <div class="col-md-3">
      <label for="sampai">Sampai Tanggal</label>
      <input type="date"  class="form-control"  name="sampai" id="sampai"></div>
   <div class="col-md-3">
   <label for="wisma">Pilih Wisma</label>
   <select class="form-control" id="exampleFormControlSelect1" name="idwisma">
                        <option value="">Pilih Wisma</option>
                        <?php
                          if(!empty($wisma))
                          {
                              foreach ($wisma as $td)
                              {
                                  ?>
                                  <option value="<?php echo $td->id_wisma ?>"
                                    <?php if($td->id_wisma == $id_wisma) { echo $selected; } ?>><?php echo $td->name ?></option>
                                  <?php
                              }
                          }
                        ?>
   </select>
   </div>
   <div class="col-md-1">
      <label for="">Aksi</label>
      <button class="btn btn-danger" type="submit">Cari</button>
   </div>
   <div class="col-md-1">
   <label for="">Cetak</label>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Cetak
      </button>
   </div>
  

</div>
</form>
<div class="card shadow">
            <div class="card-header py-3">

            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Invoice</th>
                        <th scope="col">Wisma</th>
                        <th scope="col">TglMasuk</th>
                        <th scope="col">TglKeluar</th>
                        <th scope="col">JmlKmr</th>
                        <th scope="col">Hari</th>
                        <th scope="col">Biaya</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Invoice</th>
                        <th scope="col">Wisma</th>
                        <th scope="col">TglMasuk</th>
                        <th scope="col">TglKeluar</th>
                        <th scope="col">JmlKmr</th>
                        <th scope="col">Hari</th>
                        <th scope="col">Biaya</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php $i=1; $sum = 0; foreach($chekout as $key) : ?>
							<?php $k  = strtotime($key->tgl_masuk); $p= strtotime($key->tgl_keluar); $hasil = $p - $k; $hari= floor($hasil / (60 * 60 * 24)); ?>
                             <tr class="text-sm">
                                 <td><?= $i++ ?></td>
                                 <td><?= $key->kode_invoice; ?></td>
                                 <td><?= $key->name ?></td>
                                 <td><?= $key->tgl_masuk ?></td>
                                 <td><?= $key->tgl_keluar ?></td>
                                 <td><?= $key->jml_kamar ?></td>
                                 <td><?= $hari ?></td>
                                 <td class="text-right"><?= number_format($key->total) ?></td>
							        </tr>
							<?php $sum +=$key->total;  ?>
						 <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 
<form action="<?= base_url('laporan/pdf') ?>" method="POST">
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Kondisi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="col">
         <label for="dari">Dari Tanggal</label>
         <input type="date"  class="form-control" name="dari" id="dari"></div>
         <div class="col">
         <label for="sampai">Sampai Tanggal</label>
         <input type="date"  class="form-control"  name="sampai" id="sampai"></div>
         <div class="col">
         <label for="wisma">Pilih Wisma</label>
         <select class="form-control" id="exampleFormControlSelect1" name="idwisma">
                        <option value="">Pilih Wisma</option>
                        <?php
                          if(!empty($wisma))
                          {
                              foreach ($wisma as $td)
                              {
                                  ?>
                                  <option value="<?php echo $td->id_wisma ?>"
                                    <?php if($td->id_wisma == $id_wisma) { echo $selected; } ?>><?php echo $td->name ?></option>
                                  <?php
                              }
                          }
                        ?>
            </select>
         </div>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>