<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Document</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
<body>
<h3 class="text-center">Laporan Keuangan</h3>
<?php if (!empty($dari) && !empty($sampai)) { ?>
	<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>:</td>
		<td><?= $dari ?></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>:</td>
		<td><?= $sampai ?></td>
	</tr>
</table>
<?php } else { ?>
	<table>
	<tr>
		<td>Semua Data</td>
	</tr>
</table>
<?php }?>


				<table class="table table-sm table-bordered">
  					<thead>
    					<tr>
						  <th scope="col">No</th>
						  <th scope="col">Invoice</th>
	                      <th scope="col">Wisma</th>
	                      <th scope="col">TglMasuk</th>
						  <th scope="col">TglKeluar</th>
						  <th scope="col">JmlKmr</th>
	                      <th scope="col">Hari</th>
	                      <th scope="col">Biaya</th>
    					</tr>
					</thead>
				<tbody>
						  <?php $i=1; $sum = 0; foreach($chekout as $key) : ?>
							<?php $k  = strtotime($key->tgl_masuk); $p= strtotime($key->tgl_keluar); $hasil = $p - $k; $hari= floor($hasil / (60 * 60 * 24)); ?>
                             <tr class="text-sm">
		                      <td><?= $i++ ?></td>
		                      <td><?= $key->kode_invoice; ?></td>
                              <td><?= $key->name ?></td>
                              <td><?= $key->tgl_masuk ?></td>
							  <td><?= $key->tgl_keluar ?></td>
							  <td><?= $key->jml_kamar ?></td>
                              <td><?= $hari ?></td>
                              <td class="text-right"><?= number_format($key->total) ?></td>
							</tr>
							<?php $sum +=$key->total;  ?>
						 <?php endforeach  ?>
						<tr>
							<td colspan="7" >Total</td>
							<td class="text-right"><?= number_format($sum) ?></td>
						</tr>

					</tbody>
				</table>
				

				

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>