  <!-- Footer -->
  <footer class="py-5" style="background-color:  #33C091">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?= base_url('assets/'); ?>js/jquery-ui.js"></script>
  <script src="<?= base_url('assets/'); ?>js/main.js"></script>
  <script src="<?= base_url('assets/'); ?>js/booking.js"></script>
  <script src="<?= base_url('assets/'); ?>lightbox/js/lightbox.min.js"></script>

 <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
<!-- or -->
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>

<script type="text/javascript">
 $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });

</script>

<script type="text/javascript">
  
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
    <script type="text/javascript">
    $(document).ready(function(){
    $(".preloader").fadeOut();
    })
    </script>
</body>

</html>