<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Wisma</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url('assets/'); ?>css/shop-homepage.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>css/jquery-ui.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>css/invoice.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>css/stylebook.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
  <link href="<?= base_url('assets/'); ?>/lightbox/css/lightbox.min.css" rel="stylesheet">
</head>

<body>


<div class="preloader">
			<div class="loading text-center">
        <img src="<?= base_url('assets/'); ?>img/load.gif" width="80">
        <h6>loading...........</h6>
			</div>
  </div>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top bd-navbar">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url("") ?>"><i>tek</i>MIRA</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?=base_url('home'); ?>">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url('bookwisma'); ?>">Wisma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Meeting Room</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Hall</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

