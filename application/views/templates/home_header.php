<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/venobox.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/fontawesome-free/css/all.min.css">
  

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <title>Tekmira</title>
  </head> 
  <body>


    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="<?= base_url("") ?>"><i>tek</i>MIRA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link" href="<?= base_url("") ?>">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="<?= base_url('bookwisma'); ?>">Wisma</a>
            <a class="nav-item nav-link" href="#">Meeting Room</a>
            <a class="nav-item nav-link" href="#">Hall</a>
          </div>
        </div>
      </div>
    </nav>
    <!-- akhir Navbar -->

    <div class="preloader">
			<div class="loading text-center">
				<img src="<?= base_url('assets/'); ?>img/load.gif" width="80">
        <h6>loading...........</h6>
			</div>
    </div>