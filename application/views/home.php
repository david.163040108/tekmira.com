    <!-- container -->
    <div class="container">

      <!-- info panel -->
      <div class="row justify-content-center">
        <div class="col-10 info-panel">
          <div class="row">
            <div class="col-sm">
              <img src="img/employee.svg" alt="Employee" class="img-fluid float-left">
              <h4>Wisma</h4>
              <p>Lorem ipsum dolor sit amet.</p>
            </div>
            <div class="col-lg">
              <img src="img/hires.png" alt="HiRes" class="img-fluid float-left">
              <h4> Meeting Room</h4>
              <p>Lorem ipsum dolor sit amet.</p>
            </div>
            <div class="col-lg">
              <img src="img/security.png" alt="Security" class="img-fluid float-left">
              <h4> Hall</h4>
              <p>Lorem ipsum dolor sit amet.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- akhir info panel -->


      <!-- Workingspace -->
      <div class="row workingspace workingspace2">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
          <img src="" alt="Working Space" class="img-fluid">
        </div>
        <div class="col-lg-5">
          <h2>You <span>Work</span> Like at <span>Home</span></h2>
          <p>Bekerja dengan suasana hati yang lebih asik, menyenangkan
            dan mempelajari hal baru setiap harinya.</p>
          <a href="#" class="btn btn-danger tombol">Gallery</a>
        </div>
      </div>
      <!-- akhir Workingspace -->

      <!-- Workingspace 2-->
      <div class="row workingspace">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
          <h2>You <span>Work</span> Like at <span>Home</span></h2>
          <p>Bekerja dengan suasana hati yang lebih asik, menyenangkan
            dan mempelajari hal baru setiap harinya.</p>
          <a href="#" class="btn btn-danger tombol">Gallery</a>

        </div>
        <div class="col-lg-5">
          <img src="img/workingspace.png" alt="Working Space" class="img-fluid">
        </div>
      </div>
      <!-- akhir Workingspace 2 -->


      <!-- Workingspace3 -->
      <div class="row workingspace workingspace2">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
          <img src="img/workingspace.png" alt="Working Space" class="img-fluid">
        </div>
        <div class="col-lg-5">
          <h2>You <span>Work</span> Like at <span>Home</span></h2>
          <p>Bekerja dengan suasana hati yang lebih asik, menyenangkan
            dan mempelajari hal baru setiap harinya.</p>
          <a href="#" class="btn btn-danger tombol">Gallery</a>
        </div>
      </div>
      <!-- akhir Workingspace -->



      <!-- Testimonial -->
      <section class="testimonial">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <p>"Bekerja dengan suasana hati yang lebih asik dan mempelajari hal baru setiap harinya."</p>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-6 justify-content-center d-flex">
            <img src="img/img1.png" alt="Testimonial 1">
            <img src="img/img2.png" alt="Testimonial 2" class="img-main">
            <img src="img/img3.png" alt="Testimonial 3">
          </div>
        </div>
        <div class="row justify-content-center info-text">
          <div class="col-lg text-center">
            <h5>Sunny Ye</h5>
            <p>Designer</p>
          </div>
        </div>
      </section>
      <!-- akhir Testimonial -->


    </div>
    <!-- akhir container -->


