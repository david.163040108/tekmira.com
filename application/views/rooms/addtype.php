<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
	<?= $this->session->flashdata('message'); ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Basic Card Example</h6>
            </div>
			    <div class="card-body">
			    	<?php echo form_open_multipart('kamar/add');?>
				        <div class="row">
				          <div class="col-md-4 mb-3">
				            <label for="country">Nama Kelas</label>
				           	<input type="text" class="form-control"  name="type" placeholder="Nama Kelas">
				           	<?= form_error('type', '<small class="text-danger pl-3">', '</small>'); ?>
				          </div>
				          <div class="col-md-3 mb-3">
				            <label for="state">Harga Weekend</label>
				            <div class="input-group">
					           	<div class="input-group-prepend">
					              	<span class="input-group-text">Rp.</span>
					            </div>
					            <input type="text" class="form-control" id="cc-cvv" name="weekend" placeholder="">
					          	<?= form_error('weekend', '<small class="text-danger pl-3">', '</small>'); ?>
					        </div>
				          </div>
				          <div class="col-md-3 mb-3">
				            <label for="state">Harga Weekday</label>
				           	<div class="input-group">
					           	<div class="input-group-prepend">
					              	<span class="input-group-text">Rp.</span>
					            </div>
					            <input type="text" class="form-control" id="cc-cvv" name="weekday" placeholder="">
					             	<?= form_error('weekday', '<small class="text-danger pl-3">', '</small>'); ?>
					        </div>
				          </div>
				        </div>

				        <div class="row">
						        <div class="col-sm-4">
						        	<label for="country">Gambar {Max 2mb}</label>
				                    <div class="custom-file">
				                        <input type="file" class="custom-file-input" id="image" name="userfile">
				                        <label class="custom-file-label" for="image">Choose file</label>
				                    </div>
				                </div>
				        </div>


				      	<hr class="mb-4">
				      	<div class="row">
					        <div class="col-md-9">
					        <h4 class="mb-3">Deskripsi</h4>
					           <textarea id="editor" placeholder="Silahkan Isi Deskripsi Acara"  name="deskripsi"></textarea>
					        </div>
				    	</div>

				    	<hr class="mb-4">
				    	<div class="row">
				    		<div class="col-md-4">
				    			<button class="btn btn-primary" type="submit">Submit </button>
							</div>
						</div>
					</form>
			    </div>
		</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 