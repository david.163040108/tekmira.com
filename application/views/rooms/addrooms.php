<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Basic Card Example</h6>
            </div>
			    <div class="card-body">
			    	<?php echo form_open_multipart('kamar/addkamardb');?>
				        <div class="row">
				          <div class="col-md-4 mb-3">
				            <label for="country">No Kamar</label>
				           	<input type="text" class="form-control"  name="name" placeholder="Nomor Kamar">
				           	<?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
				          </div>
				          <div class="col-md-3 mb-3">
				            <label for="state">WISMA</label>
					        	<select class="custom-select d-block w-100" id="country" name="wisma">
					              <option value="">Pilih Wisma</option>
					              <?php 
					              	if (!empty($wisma)) {
                                        foreach ($wisma as $td)
                                        {
                                            ?>
                                            <option value="<?php echo $td->id_wisma ?>"><?= $td->name ?></option>
                                            <?php
                                        }
					              	}
					               ?>
					            </select>
					           	<?= form_error('wisma', '<small class="text-danger pl-3">', '</small>'); ?>
				          </div>
				     	  <div class="col-md-3 mb-3">
				            <label for="state">Kelas Kamar</label>
					        	<select class="custom-select d-block w-100" name="kelas" required>
					              <option value="">Pilih Kelas Kamar</option>
					              <?php 
					              	if (!empty($type)) {
                                        foreach ($type as $td)
                                        {
                                            ?>
                                            <option value="<?php echo $td->id_type ?>"><?= $td->type ?></option>
                                            <?php
                                        }
					              	}

					               ?>
					            </select>
				          </div>
				          <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?>
				        </div>
					

				    	<hr class="mb-4">
				    	<div class="row">
				    		<div class="col-md-4">
				    			<button class="btn btn-primary" type="submit">Submit </button>
							</div>
						</div>
					</form>
			    </div>
		</div>


</div>
<!-- /.container-fluid -->
</div>