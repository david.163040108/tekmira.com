<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
        <?= $this->session->flashdata('message'); ?>
         <div class="card shadow">
            <div class="card-header py-3">
                <a href="<?= base_url('kamar/addtype'); ?>" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                    <i class="fas fa-file"></i>
                    </span>
                    <span class="text">Tambah Data</span>
                </a>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th>Deskrispi</th>
                      <th>Harga Weekday</th>
                      <th>Harga Weekend</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Type</th>
                      <th>Deskrispi</th>
                      <th>Harga Weekday</th>
                      <th>Harga Weekend</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php foreach($type as $td) : ?>
                    <tr>
                      <td><?= $td->type ?></td>
                      <td><?= $td->deskripsi ?></td>
                      <td><?= number_format($td->harga_weekday) ?></td>
                      <td><?= number_format($td->harga_weekend) ?></td>
                      <td class="text-center">
                        
                        <a href="<?= site_url('kamar/updatetype/'.$td->id_type.'')?>" class="btn btn-success btn-circle btn-sm">
                          <i class="fas fa-edit"></i>
                        </a> 

                        <a href="<?= site_url('kamar/deleteKelas/'.$td->id_type.'')?>" class="btn btn-danger btn-circle btn-sm" onclick="return  confirm('do you want to delete Y/N')">
                          <i class="fas fa-trash"></i>
                        </a>

                      </td>
                    </tr>
                     <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>





</div>
<!-- /.container-fluid -->
</div>

<!--   <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Apakah Anda Yakin Ingin Menghapus</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= site_url('kamar/delete/'.$td->id_type.'')?>">delete</a>
        </div>
      </div>
    </div>
  </div> -->
