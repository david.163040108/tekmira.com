<?php
$selected = "selected='selected'";
?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <?= $this->session->flashdata('message'); ?>


         <div class="card shadow ">
            <div class="card-header py-3">
              <div class="row">
                <div class="col">
                  <a href="<?= base_url('kamar/addkamar'); ?>" class="btn btn-primary btn-icon-split">
                      <span class="icon text-white-50">
                      <i class="fas fa-file"></i>
                      </span>
                      <span class="text">Tambah Data</span>
                  </a>
                </div>
                <div class="col-md-4">

                <form  action="<?= base_url('kamar/kamar') ?>" method="post" id="">
                  <div class="row">
                    <div class="col">
                      <select class="form-control" id="exampleFormControlSelect1" name="idwisma">
                        <option value="">Pilih Wisma</option>
                        <?php
                          if(!empty($wisma))
                          {
                              foreach ($wisma as $td)
                              {
                                  ?>
                                  <option value="<?php echo $td->id_wisma ?>"
                                    <?php if($td->id_wisma == $id_wisma) { echo $selected; } ?>><?php echo $td->name ?></option>
                                  <?php
                              }
                          }
                        ?>
                      </select>
                    </div>                    
                      <div class="col-md-0">
                          <button class="btn btn-primary" type="submit">ok</button>
                      </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No Kamar</th>
                      <th>Type</th>
                      <th>Wisma</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No Kamar</th>
                      <th>Type</th>
                      <th>Wisma</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php foreach($kamar as $td) : ?>
                    <tr>
                      <td><?= $td->no_kamar?></td>
                      <td><?= $td->type ?></td>
                      <td><?= $td->name ?></td>
                      <td class="text-center">
                        <?php if ($td->status == 0) { ?>
                          <a href="#" class="btn-sm btn-primary">kosong</a>

                        <?php
                         }elseif ($td->status == 2 ) { ?>
                           <a href="#" class="btn-sm btn-warning">pending</a>
                        <?php 
                         }elseif ($td->status == 1) { ?>
                           <a href="#" class="btn-sm btn-danger">booking</a> 
                         <?php } ?></td>


                      <td class="text-center">
                        <a href="<?= site_url('kamar/editkamar/'.$td->id_kamar.'')?>" class="btn btn-success btn-circle btn-sm">
                          <i class="fas fa-edit"></i>
                        </a> 
                        
                        <a href="<?= site_url('kamar/deleteKamar/'.$td->id_kamar.'')?>" class="btn btn-danger btn-circle btn-sm" onclick="return  confirm('do you want to delete Y/N')">
                          <i class="fas fa-trash"></i>
                        </a>

                      </td>
                    </tr>
                     <?php endforeach  ?>
                  </tbody>
                </table>
            </div>
          </div>



</div>
<!-- /.container-fluid -->


  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Apakah Anda Yakin Ingin Menghapus</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= site_url('kamar/deleteKamar/'.$id.'')?>">delete</a>
        </div>
      </div>
    </div>
  </div>
