<?php
$selected = "selected='selected'";
$selected2 = "selected='selected'";
?>
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <form action="<?= base_url('admin/chart') ?>" method="post">
    <div class="row">
    <div class="col-md-4">
            <select name="tahun" class="custom-select" id="inputGroupSelect04" >
                <option value="">Please Select</option>
                <?php
                $thn_skr = date('Y');
                for ($x = $thn_skr; $x >= 2010; $x--) {
                ?>
                    <option value="<?php echo $x ?>"
                    <?php if($x == $tahun) { echo $selected2; } ?>><?php echo $x ?></option>
                <?php
                }
                ?>
            </select>
    </div>
    <div class="col-md-4">

            <div class="input-group">
                <select class="custom-select" id="inputGroupSelect04" name="wisma">
                    <option value="">Choose...</option>
                    <?php
                          if(!empty($wisma))
                          {
                              foreach ($wisma as $td)
                              {
                                  ?>
                                  <option value="<?php echo $td->id_wisma ?>"
                                    <?php if($td->id_wisma == $id_wisma) { echo $selected; } ?>><?php echo $td->name ?></option>
                                  <?php
                              }
                          }
                        ?>
                </select>
                    <div class="input-group-append">
                        <button class="btn btn-danger" type="submit">Button</button>
                    </div>
            </div>
        
    </div>
     
    </div>
    </form>   
    <!-- chart -->
    <div id="custom"></div>


</div>
</div>