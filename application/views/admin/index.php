<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

<div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="<?= base_url('admin/kamarkosong'); ?>">Kamar Kosong </a>  </div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $kosong ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-person-booth fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><a href="<?= base_url('admin/kamarterbooking'); ?>">Kamar TerBooking</a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $booking ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-person-booth fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1"><a href="<?= base_url('admin/chart'); ?>">Chart</a></div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-chart-line fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1"><a href="<?= base_url('admin/kamarpending'); ?>">Kamar Pending</a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $pending ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-business-time fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <h6>Data Yang Menginap <a href="">detail</a> <hr></h6>
            <div id="chart"></div>

</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content --> 