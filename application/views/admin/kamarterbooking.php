<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

<!--  -->

<div class="row">
	<?php foreach ($wisma as $td ) { ?>
	<div class="col-md-4">
		<div class="list-group">
		  <a href="#" class="list-group-item list-group-item-action bg-success text-white">
		    <?= $td->name  ?>
		  </a>
		  <?php foreach ($kosong as $key) {
		  	
			if ($td->id_wisma == $key->id_wisma) { ?>

		  <a href="#" class="list-group-item list-group-item-action">
		  	<?= $key->type ?>
		  	<span class="badge badge-warning"><?= $key->total ?></span>
		  </a>
		<?php }} ?>
		</div>	
	</div>
<?php } ?>
</div>



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 