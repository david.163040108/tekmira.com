<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_model extends CI_Model {

		//untuk tampilan booking
		public function get_all($id, $type){
			$query = $this->db->query("SELECT tb_kamar.id_type, tb_type_kamar.type, tb_kamar.id_type, COUNT( * ) as total FROM tb_kamar
				JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_kamar.id_type Where status = 0 AND id_wisma = '$id' AND tb_kamar.id_type = '$type'
                 GROUP BY id_type 
				");
			return $query->result();
		}

		//chart
		public function chart(){
			$query = $this->db->query("SELECT tbl_wisma.name ,COUNT( * ) as total FROM `tb_book` JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_book.id_wisma WHERE status = 0 GROUP BY tb_book.id_wisma");
			return $query->result();

			//query bulan tahun
			// SELECT tbl_wisma.name,DATE_FORMAT(tgl_masuk,'%M %Y') AS bulan, COUNT(*) AS total FROM `tb_book` JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_book.id_wisma GROUP BY tb_book.id_wisma
		}
		//chart line
		public function chartline(){
			$t = date('Y');
			$this->db->select('tbl_wisma.name, DATE_FORMAT(tgl_masuk, "%M %Y") as bulan, count(*) as total');
			$this->db->from('tb_book');
			$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');

			$this->db->where('YEAR(tgl_masuk) ', $t);
			$this->db->group_by("bulan");
			$this->db->order_by('tgl_masuk'); 

			$query = $this->db->get();
			return $query;
		}

		// chart view
		public function customChart($wisma,$tahun){
			$t = date('Y');
			$this->db->select('tbl_wisma.name, DATE_FORMAT(tgl_masuk, "%M %Y") as bulan, count(*) as total');
			$this->db->from('tb_book');
			$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');

			
			if (!empty($wisma) && !empty($tahun)) {
				# code...
				$this->db->where('tb_book.id_wisma', $wisma);
				$this->db->where('YEAR(tgl_masuk) ', $tahun);
			}else if(empty($wisma) && empty($tahun)){
				$this->db->where('YEAR(tgl_masuk) ', $t);
			}
			$this->db->group_by("bulan");
			$this->db->order_by('tgl_masuk'); 

			$query = $this->db->get();
			return $query;
		}

		// get invoice
		public function get_invoice($kode){
			$query = $this->db->query("SELECT * FROM tabel_booking_konfirmasi JOIN tb_type_kamar ON tb_type_kamar.id_type = tabel_booking_konfirmasi.id_type JOIN tbl_wisma ON tbl_wisma.id_wisma = tabel_booking_konfirmasi.id_wisma WHERE kode_invoice = '$kode'");
			return $query->result();
		}
//===============================================================get notifikasi ===============================================
		public function get_notif(){
			$query = $this->db->get_where('tabel_booking_konfirmasi', array('status' => 0));
			if($query->num_rows()>0){
			
			return $query->num_rows();
			
			}else{
			
			return 0;
			
			}
		}
//============================================================ get konfirmasi all ==================================
	public function get_konfirmasi(){
		$query = $this->db->get_where('tabel_booking_konfirmasi',  array('status' => 0 ));
		return $query->result();
	}

	public function get_konfirmasi_wisma1(){
		$query = $this->db->get_where('tabel_booking_konfirmasi',  array('status' => 0 , 'id_wisma' => 1));
		return $query->result();
	}
	public function get_konfirmasi_wisma2(){
		$query = $this->db->get_where('tabel_booking_konfirmasi',  array('status' => 0 , 'id_wisma' => 2));
		return $query->result();
	}

	public function get_konfirmasi_wisma3(){
		$query = $this->db->get_where('tabel_booking_konfirmasi',  array('status' => 0 , 'id_wisma' => 3));
		return $query->result();
	}
	//============================================== get detail konfirmasi booking ============================
	public function getdetailkonfirmasi($id)
	{
				$query = $this->db->query("SELECT * FROM tabel_booking_konfirmasi JOIN tb_type_kamar ON tb_type_kamar.id_type = tabel_booking_konfirmasi.id_type JOIN tbl_wisma ON tbl_wisma.id_wisma = tabel_booking_konfirmasi.id_wisma WHERE id_booking = '$id'");
			return $query->row();
	}

	public function get_detailbook_by_book($id_book){
		$this->db->select('*');
		$this->db->from('tb_book');
		$this->db->join('tb_detail_book', 'id_book=id_book');
		$this->db->join('tb_kamar', 'id_kamar=id_kamar');
		$this->db->where('id_book',$id_book);
		$query = $this->db->get();
		return $query;
	}

	
	//=========================================================== add booking ======================================================
		// CREATE
	function add_book($data,$kamar){

		$this->db->trans_start();

			$this->db->insert('tb_book', $data);
		
			$id_book = $this->db->insert_id();
			$result = array();
			    foreach($kamar AS $key => $val){
				     $result[] = array(
				      'id_book'  	=> $id_book,
				      'id_kamar'  	=> $_POST['kamar'][$key]
				     );
			    } 			


			//MULTIPLE INSERT TO DETAIL TABLE
			$this->db->insert_batch('tb_detail_book', $result);

			    foreach($kamar AS $key => $val){
				     $up= array(
				      'status'  	=> 1,
				     );

				     $this->db->where('id_kamar', $_POST['kamar'][$key]);
				     $this->db->update('tb_kamar', $up);

			    } 
			    				     
			     
		$this->db->trans_complete();

	}

	public function getList($id)
	{
		return $query = $this->db->get_where('tabel_booking_konfirmasi', array('id_book' => $id ))->row();
	}
//=================================get daftar book  all=======================================================
	public function getListBook() {

		 $this->db->select('*');
		 $this->db->from('tb_book');
		 $this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		 $this->db->group_by("tb_book.id_book");
		 $this->db->where('status', 0);
		 $query = $this->db->get();
		 return $query->result();
	}

	public function getListBook_tekmira2() {

		$this->db->select('*');
		$this->db->from('tb_book');
		$this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		$this->db->group_by("tb_book.id_book");
		$this->db->where('status', 0);
		$this->db->where('tb_book.id_wisma', 2);
		$query = $this->db->get();
		return $query->result();
   }

		public function getListBook_tekmira1() {

		$this->db->select('*');
		$this->db->from('tb_book');
		$this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		$this->db->group_by("tb_book.id_book");
		$this->db->where('status', 0);
		$this->db->where('tb_book.id_wisma', 1);
		$query = $this->db->get();
		return $query->result();
		}
		public function getListBook_tekmira3() {

		$this->db->select('*');
		$this->db->from('tb_book');
		$this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		$this->db->group_by("tb_book.id_book");
		$this->db->where('status', 0);
		$this->db->where('tb_book.id_wisma', 2);
		$query = $this->db->get();
		return $query->result();
		}
//================================================================================================================
	//Daftar List Chekout

	public function getListChekout($id_wisma) {

		 $this->db->select('*');
		 $this->db->from('tb_book');
		 $this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		 $this->db->group_by("tb_book.id_book");
		 $this->db->where('status', 1);
		 if (!empty($id_wisma)) {
			$this->db->where('tb_book.id_wisma', $id_wisma);
		 }

		 $query = $this->db->get();
		 return $query->result();
	}

	public function laporan($id_wisma,$dari,$sampai) {

		 $this->db->select('*');
		 $this->db->from('tb_book');
		 $this->db->join('tb_detail_book','tb_detail_book.id_book=tb_book.id_book');
		 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		 $this->db->group_by("tb_book.id_book");
		 $this->db->where('status', 1);
		 if (!empty($id_wisma)) {
			$this->db->where('tb_book.id_wisma', $id_wisma);
		 }
		 if (!empty($dari)) {
			$this->db->where('tb_book.tgl_masuk >=', $dari);
		 }
		 if (!empty($sampai)) {
			$this->db->where('tb_book.tgl_keluar <=', $sampai);
		 }
		 $query = $this->db->get();
		 return $query->result();
	}
 //===================================== get detail book json ==========================================================
	public function get_detailbook($id){

		$query = $this->db->query("SELECT * FROM tb_book JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_book.id_type JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_book.id_wisma JOIN tb_detail_book ON tb_detail_book.id_book = tb_book.id_book JOIN tb_kamar ON tb_kamar.id_kamar = tb_detail_book.id_kamar WHERE tb_book.id_book = '$id'");
			return $query->result();

	}

//==================================== get chekout json ================================================================


// =================================== proses perpanjang ==========================================================

	public function perpanjang($data1,$data2,$id ){
		
				$this->db->insert('tb_perpanjang', $data1);

				$this->db->where('id_book', $id);
		$status = $this->db->update('tb_book', $data2);

		if ($status) {
			 $this->session->set_flashdata('allbook', '<div class="alert alert-success" role="alert">Berhasil Perpanjang</div>');
			redirect('Booking/buktiperpanjangan/'.$id.'');
		}else{
			$this->session->set_flashdata('allbook', '<div class="alert alert-danger" role="alert">Gagal Perpanjang</div>');
			redirect('Booking/listbooking');
		}
	}

	// untuk membatalkan pemesanan yang belum di konfirmasi
	public function pembatalanbook($id_type,$id_wisma,$jmlkmr,$id){


				$statuskamar = array('status' => 0);
                $array = array('id_type' => $id_type, 'id_wisma' => $id_wisma, 'status' => 2);
                $this->db->where($array);
                $this->db->order_by('id_kamar', 'DESC');
                $this->db->limit($jmlkmr);
                $this->db->update('tb_kamar', $statuskamar);
                
                $statusbook = array('status' => 2);
                $this->db->where('id_booking', $id);
                $this->db->update('tabel_booking_konfirmasi', $statusbook);
	}

	// Menampilkan invoice berdasarkan id
	public function getperpanjang($id){

		$this->db->select('*');
		$this->db->from('tb_book');
		$this->db->join('tb_perpanjang','tb_perpanjang.id_book=tb_book.id_book');
		$this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_book.id_wisma');
		$this->db->join('tb_type_kamar','tb_type_kamar.id_type=tb_book.id_type');
		$this->db->where('tb_perpanjang.id_book', $id);
		$this->db->order_by('tb_perpanjang.id_perpanjang','DESC');    
		$this->db->limit(1);   
		$query = $this->db->get();
		return $query->result();
	}
}