<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_model extends CI_Model {

	//
	public function getType()
	{
		return $this->db->get('tb_type_kamar')->result();
	}

	//get booking pending
	public function getbookingPending(){
		$query = $this->db->get_where('tb_kamar', array('status' => 2));
		
		if($query->num_rows()>0){
		
		return $query->num_rows();
		
		}else{
		
		return 0;
		
		}
	}

//=============================================== get kamar kosong all wisma ========================
	public function getkamarkosong(){
		$query = $this->db->get_where('tb_kamar', array('status' => 0));
		
		if($query->num_rows()>0){
		
		return $query->num_rows();
		
		}else{
		
		return 0;
		
		}
	}

	//========================================get kamar  kosong berdasarkan wisma ========================

		public function getberdasarkanwisma(){
			$query = $this->db->query("SELECT tbl_wisma.id_wisma,tb_kamar.id_type, tb_type_kamar.type,tb_type_kamar.img_type,tb_type_kamar.deskripsi,tb_type_kamar.harga_weekday, tb_type_kamar.harga_weekend, tb_kamar.id_type, COUNT( * ) as total FROM tb_kamar JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_kamar.id_wisma
				JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_kamar.id_type Where status = 0 
                 GROUP BY tb_kamar.id_wisma , tb_kamar.id_type 
				");
			return $query->result();
		}		

//=========================================get kamar yang sudah terbooking berdasarkan wisam ============================
		public function getlistkamarbooking(){
			$query = $this->db->query("SELECT tbl_wisma.id_wisma,tb_kamar.id_type, tb_type_kamar.type,tb_type_kamar.img_type,tb_type_kamar.deskripsi,tb_type_kamar.harga_weekday, tb_type_kamar.harga_weekend, tb_kamar.id_type, COUNT( * ) as total FROM tb_kamar JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_kamar.id_wisma
				JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_kamar.id_type Where status = 1 
                 GROUP BY tb_kamar.id_wisma , tb_kamar.id_type 
				");
			return $query->result();
		}

// =========================== get kamar yang terpending ====================================
		public function getlistkamarpending(){
			$query = $this->db->query("SELECT tbl_wisma.id_wisma,tb_kamar.id_type, tb_type_kamar.type,tb_type_kamar.img_type,tb_type_kamar.deskripsi,tb_type_kamar.harga_weekday, tb_type_kamar.harga_weekend, tb_kamar.id_type, COUNT( * ) as total FROM tb_kamar JOIN tbl_wisma ON tbl_wisma.id_wisma = tb_kamar.id_wisma
				JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_kamar.id_type Where status = 2 
                 GROUP BY tb_kamar.id_wisma , tb_kamar.id_type 
				");
			return $query->result();
		}
//===========================================================================================================
	//get kamar booking
	public function getkamarbooking(){
		$query = $this->db->get_where('tb_kamar', array('status' => 1));
		
		if($query->num_rows()>0){
		
		return $query->num_rows();
		
		}else{
		
		return 0;
		
		}
	}


	public function deleteType($id) {
		$_id = $this->db->get_where('tb_type_kamar',['id_type' => $id])->row();
		$this->db->where('tb_type_kamar.id_type', $id);
		$this->db->delete('tb_type_kamar');
		unlink("assets/img/kamar/".$_id->img_type);
		return $this->db->affected_rows();
	}

		public function getdetailtype($id)
	{
		return $this->db->get_where('tb_type_kamar', array('id_type' => $id))->row();
	}

		public function updatetype($detail)
    {
        $this->db->where('id_type', $detail['kamar']);
        return $this->db->update('tb_kamar', $detail);
    }

//=================================================kamar==========================================

	public function getKamar($id_wisma) {
	 $this->db->select('*');
	 $this->db->from('tb_kamar');
	 $this->db->join('tb_type_kamar','tb_type_kamar.id_type=tb_kamar.id_type');
	 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_kamar.id_wisma');
	 if (!empty($id_wisma)) {
	 	$this->db->where('tb_kamar.id_wisma', $id_wisma);
	 }
	 $query = $this->db->get();
	 return $query->result();
	}

//=================================================================================================


	public function getkamarpending($jmlkmr,$id_wisma) {
	 $this->db->select('*');
	 $this->db->from('tb_kamar');
	 $this->db->join('tb_type_kamar','tb_type_kamar.id_type=tb_kamar.id_type');
	 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_kamar.id_wisma');
	 $this->db->where('status',2);
	 $this->db->where('tb_kamar.id_wisma',$id_wisma);
	 $this->db->limit($jmlkmr);
	 $query = $this->db->get();
	 return $query->result();
	}
//===================================================================================================

	public function deleteKamar($id){
		$this->db->where('id_kamar', $id);
		$this->db->delete('tb_kamar');
		return $this->db->affected_rows();
	}
//===================================================================================================

	public function getdetailkamar($id)
	{
		return $this->db->get_where('tb_kamar', array('id_kamar' => $id))->row();
	}
//==================================================================================================

	public function update($detail)
    {
        $this->db->where('id_kamar', $detail['kamar']);
        return $this->db->update('tb_kamar', $detail);
    }


// ============================================ 

}