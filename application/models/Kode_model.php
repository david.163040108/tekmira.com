<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode_model extends CI_Model {

		//kode invoice
		public function kode(){
		  $this->db->select('RIGHT(tabel_booking_konfirmasi.kode_invoice,1) as kode_invoice', FALSE);
		  $this->db->order_by('kode_invoice','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('tabel_booking_konfirmasi');  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->kode_invoice) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  }
			  $tgl=date('dmY'); 
			  $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			  $kodetampil = "TM"."3".$tgl.$batas;  //format kode
			  return $kodetampil;  
		 }

}