<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisma_model extends CI_Model {

	//getwisma
	public function getWisma(){
		return $this->db->get('tbl_wisma')->result();
	}

	//deletewiwisma
	public function deletewismadb($id) {
		$_id = $this->db->get_where('tbl_wisma',['id_wisma' => $id])->row();
		$this->db->where('id_wisma', $id);
		$this->db->delete('tbl_wisma');
		unlink("assets/img/wisma/".$_id->image);
		return $this->db->affected_rows();
	}

	//get detail
	public function getdetailwisma($id)
	{
		return $this->db->get_where('tbl_wisma', array('id_wisma' => $id))->row();
	}

	//update
	public function update($detail)
    {
        $this->db->where('id_kamar', $detail['kamar']);
        return $this->db->update('tb_kamar', $detail);
    }

	//getkamar
    public function getKamar() {
	 $this->db->select('*');
	 $this->db->from('tb_kamar');
	 $this->db->join('tb_type_kamar','tb_type_kamar.id_type=tb_kamar.id_type');
	 $this->db->join('tbl_wisma','tbl_wisma.id_wisma=tb_kamar.id_wisma');
	 $this->db->where('status', 0);
	 $this->db->where('tbl_wisma.id_wisma', 1);

	 $query = $this->db->get();
	 return $query->result();
	}


		public function get_all($id){
			$query = $this->db->query("SELECT tb_kamar.id_type, tb_type_kamar.type,tb_type_kamar.img_type,tb_type_kamar.deskripsi,tb_type_kamar.harga_weekday, tb_type_kamar.harga_weekend, tb_kamar.id_type, COUNT( * ) as total FROM tb_kamar
				JOIN tb_type_kamar ON tb_type_kamar.id_type = tb_kamar.id_type Where status = 0 AND id_wisma = '$id'
                 GROUP BY id_type 
				");
			return $query->result();
		}

}