<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	//================get data gallery ===========================================
    public function getgallery()
	{
		return $this->db->get('tb_gallery')->result();
	}

	//=============== delete data gallery ========================================
	public function deleteGallery($id){
		$_id = $this->db->get_where('tb_gallery',['id' => $id])->row();
		$this->db->where('id', $id);
		$this->db->delete('tb_gallery');
		unlink("assets/img/gallery/".$_id->img);
		return $this->db->affected_rows();
	}

	//==================== get data promo ==========================================
	public function getPromo(){
		return $this->db->get('tb_promo_img')->result();
	}

	//====================== get delete promo =======================================
	public function deletePromo($id){
		$_id = $this->db->get_where('tb_promo_img',['id' => $id])->row();
		$this->db->where('id', $id);
		$this->db->delete('tb_promo_img');
		unlink("assets/img/baner/".$_id->img);
		return $this->db->affected_rows();
	}

	//========================== menghitung jumlah iklan ============================
	public function jumlahIklan(){
		$query = $this->db->get('tb_promo_img');
		if($query->num_rows()>0){
		return $query->num_rows();
		}else{
		return 0;
		}
	}

}