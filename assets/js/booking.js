
var i;
var mD = {
  first: '',
  scnd: '',
  init: function(){
    this.setFirst();
    this.setSecond();
  },
  setFirst: function(){
    var _this = this;
    $("#first").datepicker({
      onSelect: function(){
        _this.first = $(this).val();
        _this.calcDiff();
      }

    });
  },
  setSecond: function(){
    var _this = this;
    $("#second").datepicker({
      onSelect: function(){
        _this.scnd = $(this).val();
      //  $("#tgl_akhir").html(_this.scnd);
        _this.calcDiff();
        $('#kamar').prop('selectedIndex',0); 
      }
    });
  },
  calcDiff: function(){
    var date1 = new Date(this.first);
    var date2 = new Date(this.scnd);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if(date1 > date2){
      $("#jw").html("Salah");
      $("#first").css({"background-color":"red","color":"white"});
      $("#durasi").val("Salah"); 
    }else if (diffDays == 0) {
      $("#first").css({"background-color":"red","color":"white"});
      $("#jw").html('Salah');
      $("#durasi").val("Salah"); 
    } 
    else {
      $("#first").css({"background-color":"","color":""});
       var hari = date1.getDay();
      if (hari === 0 || hari === 6) {
        var c = $("#weekend").val();
        console.log(c);
        console.log('weekend');
        $("#harga").html(c);
      }else{
        var c = $("#weekday").val();
        console.log(c);
        console.log('weekday');
        $("#harga").html(c);
      }
      $("#jw").html(diffDays);  
      $("#durasi").val(diffDays);    
      $('#kamar').prop('selectedIndex',0);   
    }
  }
}

mD.init();

$(document).ready(function(){
  $("select.kamar").change(function(){
      var kamar = $(this).children("option:selected").val();
      $('#jmlkmr').html(kamar);
      var harga = $("#harga").html();
      var total = kamar * harga;
      var malam = $("#jw").html(); 
      var  subtotal = total*malam;
       $('#hs').html(subtotal);
       $('#hasil').val(subtotal);

  var bilangan = subtotal;

  var number_string = bilangan.toString(),
    sisa  = number_string.length % 3,
    rupiah  = number_string.substr(0, sisa),
    ribuan  = number_string.substr(sisa).match(/\d{3}/g);
      
  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  } 
      $('#total').html(rupiah);
      
  });
});