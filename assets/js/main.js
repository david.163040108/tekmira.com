(function ($) {

    $(window).on('load', function () {
      var galleryIsotope = $('.gallery-container').isotope({
        itemSelector: '.gallery-item'
      });
      $('#gallery-flters li').on( 'click', function() {
        $("#gallery-flters li").removeClass('filter-active');
        $(this).addClass('filter-active');
    
        galleryIsotope.isotope({ filter: $(this).data('filter') });
      });
    });
    
  })(jQuery);
  
  