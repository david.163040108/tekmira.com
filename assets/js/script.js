$(function(){

    $('.detail').on('click', function(){

        const id =$(this).data('id');
       // console.log(id);
        $.ajax({
            url: 'http://localhost/tekmira.com/booking/getdetail',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
                $('#kode').val(data.kode_invoice);
                $('#nama').val(data.nama);
                $('#nohp').val(data.telphone);
                $('#email').val(data.email);

                $('#tgl_masuk').val(data.tgl_masuk);
                $('#tgl_keluar').val(data.tgl_keluar);
                $('#jml_kamar').val(data.jumlah_kamar);
                $('#durasi').val(data.durasi);
                $('#tipe').val(data.type);
                $('#wisma').val(data.name);
                $('#total').val(data.total);
                console.log(data);
            }
        });

    })



        $('.book').on('click', function(){

        const id =$(this).data('id');
        $.ajax({
            url: 'http://localhost/tekmira.com/booking/getdetilbook',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
            console.log(data);
                var text= "";
                for (var i = 0; i < data.length; i++) {

                    $('#kode').val(data[i]['kode_invoice']);
                    $('#nama').val(data[i]['nama']);
                    $('#nohp').val(data[i]['no_hp']);
                    $('#email').val(data[i]['email']);
                    $('#tgl_masuk').val(data[i]['tgl_masuk']);
                    $('#tgl_keluar').val(data[i]['tgl_keluar']);
                    $('#jml_kamar').val(data[i]['jml_kamar']);
                    $('#tipe').val(data[i]['type']);
                    $('#wisma').val(data[i]['name']);
                    $('#total').val(data[i]['total']);
                    $('#waktu').val(data[i]['tgl_proses']);
                       text += data[i]['no_kamar']  + " ";
                }
        
                $('#no_kmr').val(text); 


            }
        });

    })

        $('.book2').on('click', function(){

        const id =$(this).data('id');
            // console.log(id);
        $.ajax({
            url: 'http://localhost/tekmira.com/booking/getdetilchekout',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
            // console.log(data);
                var text= "";
                for (var i = 0; i < data.length; i++) {

                    $('#kode').val(data[i]['kode_invoice']);
                    $('#nama').val(data[i]['nama']);
                    $('#nohp').val(data[i]['no_hp']);
                    $('#email').val(data[i]['email']);
                    $('#tgl_masuk').val(data[i]['tgl_masuk']);
                    $('#tgl_keluar').val(data[i]['tgl_keluar']);
                    $('#jml_kamar').val(data[i]['jml_kamar']);
                    $('#tipe').val(data[i]['type']);
                    $('#wisma').val(data[i]['name']);
                    $('#total').val(data[i]['total']);
                       text += data[i]['no_kamar']  + " ";
                }
                // console.log(text);
        
                $('#no_kmr').val(text); 


            }
        });

    })

    //print 
    $('#printInvoice').on('click', function(){
        $(".hilang").hide();
        $("#accordionSidebar").hide();
        Popup($('.invoice')[0].outerHTML);
        $(".hilang").show();
        $("#accordionSidebar").show();
        function Popup(data) 
        {
            window.print();
            return true;
        }
    })

//notifikasi
    $.getJSON( "http://localhost/tekmira.com/admin/notifikasi", function( data ) {
        $('#notif').html(data);
        console.log(data);


        });
});

//fungsi date
$(function() {
    $( "#datepicker" ).datepicker();
});

    window.onload=function(){
        $('#datepicker').on('change', function() {
            var keluar =  $("#tglkeluar").val();
            var jmlkamar = $("#jmlkamar").val();
            // var harga = $("#harga").val();
            // var total = jmlkamar * harga;
            //======================================
            var perpanjang = new Date(this.value);
            var tglkeluar = new Date(keluar);
            var timeDiff = Math.abs(perpanjang.getTime() - tglkeluar.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if(tglkeluar > perpanjang){
            $("#hasil").val("Salah");
            $("#total").val('Salah');
            }else if (diffDays == 0) {
            $("#hasil").val('Salah');
            $("#total").val('Salah');
            } 
            else {
                var hari = perpanjang.getDay();
                if (hari === 0 || hari === 6) {
                  var c = $("#weekend").val();
                  
                  console.log(c);
                  console.log('weekend');
                   $("#harga").val(c);

                  var total = jmlkamar * c; 
                  var subtotal = total * diffDays;

                  $("#total").val(subtotal);

                  var number_string = subtotal.toString(),
                  sisa  = number_string.length % 3,
                  rupiah  = number_string.substr(0, sisa),
                  ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                  if (ribuan) {
                  separator = sisa ? '.' : '';
                  rupiah += separator + ribuan.join('.');
                  } 
                  $("#hasil").val(rupiah);   
                }else{
                  var c = $("#weekday").val();
                  console.log(c);
                  console.log('weekday');
                  $("#harga").val(c);

                  var total = jmlkamar * c; 
                  var subtotal = total * diffDays;

                  $("#total").val(subtotal);

                  var number_string = subtotal.toString(),
                  sisa  = number_string.length % 3,
                  rupiah  = number_string.substr(0, sisa),
                  ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                  if (ribuan) {
                  separator = sisa ? '.' : '';
                  rupiah += separator + ribuan.join('.');
                  } 
                  $("#hasil").val(rupiah);   ;
                }
            }
        });
        }

