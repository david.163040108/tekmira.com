-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2019 at 06:57 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tekmira`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_booking_konfirmasi`
--

CREATE TABLE `tabel_booking_konfirmasi` (
  `id_booking` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telphone` int(12) NOT NULL,
  `email` int(20) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `tgl_keluar` date NOT NULL,
  `jumlah_kamar` int(3) NOT NULL,
  `type_kamar` int(1) NOT NULL,
  `tamu` int(3) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wisma`
--

CREATE TABLE `tbl_wisma` (
  `id_wisma` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wisma`
--

INSERT INTO `tbl_wisma` (`id_wisma`, `name`, `alamat`, `no_telp`, `image`) VALUES
(1, 'Wisma Tekmira 1', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'testing.jpg'),
(2, 'Wisma Tekmira 2', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'kamar1.jpg'),
(3, 'Wisma Tekmira 3', 'Jl. Jend. Sudirman No.623, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40211', '(022) 603048', 'kamar2.jpg'),
(5, 'Wisma Tekmira 4', 'Blk Pejagan Asem Rt02 Rw01 Desa KedungBunder Kecamatan Gempol\r\nRumah H. Hasan/Hj. Sri', '(022) 603049', 'SH9co.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_book`
--

CREATE TABLE `tb_book` (
  `id_book` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_hp` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_book`
--

CREATE TABLE `tb_detail_book` (
  `id_detail_book` int(11) NOT NULL,
  `id_book` int(11) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `tgl_cheekin` date NOT NULL,
  `tgl_cheekout` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_wisma` int(11) NOT NULL,
  `no_kamar` varchar(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `id_type`, `id_wisma`, `no_kamar`, `status`) VALUES
(12, 1, 1, 'C01', 0),
(17, 2, 1, 'C01', 0),
(18, 3, 1, 'C01', 0),
(19, 1, 1, 'C02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_type_kamar`
--

CREATE TABLE `tb_type_kamar` (
  `id_type` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `harga_weekday` int(11) NOT NULL,
  `harga_weekend` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_type_kamar`
--

INSERT INTO `tb_type_kamar` (`id_type`, `type`, `deskripsi`, `harga_weekday`, `harga_weekend`) VALUES
(1, 'Standar', 'testing', 175000, 150000),
(2, 'Deluxe', 'testing', 225000, 200000),
(3, 'Superior', 'testing', 200000, 175000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(14, 'david', 'davidpriyadi77@gmail.com', 'kamar.jpg', '$2y$10$Y9S9..WEsWudycsWofNeIe.DvyWb0uSHPiPVHXuoEEU5WIqY8YlTK', 1, 1, 1562167212);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(9, 1, 4),
(10, 1, 5),
(12, 1, 6),
(13, 1, 7),
(16, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Kamar'),
(5, 'Booking'),
(6, 'Bukti Pembayaran'),
(7, 'Wisma');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'My Profile', 'user', 'fas fa-fw fa-user', 1),
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(7, 1, 'Role', 'admin/role', 'fas fa-fw fa-user-tie', 1),
(8, 2, 'Change Password', 'user/changepassword', 'fas fa-fw fa-key', 1),
(9, 4, 'Kamar', 'kamar/kamar', 'fas fa-fw fa-person-booth', 1),
(11, 4, 'Kelas Kamar', 'kamar/kelaskamar', 'fas fa-fw fa-star', 1),
(12, 5, 'Pemesanan Baru', 'booking/pemesananbaru', 'fas fa-fw fa-calendar-alt', 1),
(13, 5, 'Semua Pesanan', 'booking/allboking', 'fas fa-fw fa-calendar-check', 1),
(14, 6, 'Bukti Transfer', 'buktipembayaran', 'fas fa-fw fa-receipt', 1),
(15, 7, 'Wisma', 'wisma', 'fas fa-fw fa-calendar-alt', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_booking_konfirmasi`
--
ALTER TABLE `tabel_booking_konfirmasi`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indexes for table `tbl_wisma`
--
ALTER TABLE `tbl_wisma`
  ADD PRIMARY KEY (`id_wisma`);

--
-- Indexes for table `tb_book`
--
ALTER TABLE `tb_book`
  ADD PRIMARY KEY (`id_book`);

--
-- Indexes for table `tb_detail_book`
--
ALTER TABLE `tb_detail_book`
  ADD PRIMARY KEY (`id_detail_book`);

--
-- Indexes for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`),
  ADD KEY `id_type` (`id_type`),
  ADD KEY `id_wisma` (`id_wisma`);

--
-- Indexes for table `tb_type_kamar`
--
ALTER TABLE `tb_type_kamar`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_booking_konfirmasi`
--
ALTER TABLE `tabel_booking_konfirmasi`
  MODIFY `id_booking` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_wisma`
--
ALTER TABLE `tbl_wisma`
  MODIFY `id_wisma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_book`
--
ALTER TABLE `tb_book`
  MODIFY `id_book` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_book`
--
ALTER TABLE `tb_detail_book`
  MODIFY `id_detail_book` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_type_kamar`
--
ALTER TABLE `tb_type_kamar`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD CONSTRAINT `tb_kamar_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `tb_type_kamar` (`id_type`),
  ADD CONSTRAINT `tb_kamar_ibfk_2` FOREIGN KEY (`id_wisma`) REFERENCES `tbl_wisma` (`id_wisma`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
